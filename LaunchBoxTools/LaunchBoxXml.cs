﻿using LaunchBoxTools.Dialogs;
using LaunchBoxTools.NamingConventions;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Xml.Linq;

namespace LaunchBoxTools
{
    /// <summary>
    /// Wrapper for LaunchBox data.
    /// </summary>
    static class LaunchBoxData
    {

        #region Enumerations

        /// <summary>
        /// The possible results from iterating games.
        /// </summary>
        public enum IterateOverGamesResult { NeverStarted, Canceled, Error, Finished };

        #endregion

        #region Properties

        #region Static Properties

        /// <summary>
        /// Get a random number generator.
        /// </summary>
        private static Random Random { get; } = new Random();

        /// <summary>
        /// Get the list of all platforms.
        /// </summary>
        public static ImmutableHashSet<string> Platforms { get; set; }

        /// <summary>
        /// Get or set the list of selected platforms.
        /// </summary>
        public static IEnumerable<string> SelectedPlatforms
        {
            get { return _SelectedPlatforms.ToImmutableHashSet(); }

            set
            {
                _SelectedPlatforms.Clear();
                if (value != null)
                    foreach (var platform in value.Where(p => Platforms.Contains(p)))
                        _SelectedPlatforms.Add(platform);
            }
        }
        private static HashSet<string> _SelectedPlatforms = new HashSet<string>();

        /// <summary>
        /// The directory of the launchbox settings.
        /// </summary>
        public static string LaunchBoxDirectory { get; private set; }

        /// <summary>
        /// The platform settings document.
        /// </summary>
        public static XDocument PlatformList { get; private set; }

        /// <summary>
        /// A backup of the platform settings documents.
        /// </summary>
        public static XDocument PlatformListBackup { get; private set; }

        /// <summary>
        /// The LaunchBox settings document.
        /// </summary>
        public static XDocument LaunchBoxSettings { get; private set; }
        
        /// <summary>
        /// The document for each platform game list.
        /// </summary>
        public static Dictionary<string, XDocument> GameLists { get; private set; }

        /// <summary>
        /// A backup for each platform game list.
        /// </summary>
        public static Dictionary<string, XDocument> GameListBackups { get; private set; }

        #endregion

        #region IsSetup DependencyObject

        /// <summary>
        /// Wrapper for the IsSetup property so it can be databound. Stupid, I know.
        /// </summary>
        public class IsSetupDependencyObject : DependencyObject
        {
            /// <summary>
            /// Property for IsSetup.
            /// </summary>
            public static readonly DependencyProperty IsSetupProperty =
                DependencyProperty.Register("IsSetup", typeof(bool), typeof(IsSetupDependencyObject), new UIPropertyMetadata(false));

            /// <summary>
            /// Get the current instance of LaunchBoxXml.
            /// </summary>
            public bool IsSetup
            {
                get { return (bool)GetValue(IsSetupProperty); }
                set { SetValue(IsSetupProperty, value); }
            }
        };

        /// <summary>
        /// The object to bind to inorder to track IsSetup.
        /// </summary>
        public static IsSetupDependencyObject IsSetupBindingObject { get; } = new IsSetupDependencyObject();

        #endregion

        #region Xml Structure

        #region General

        /// <summary>
        /// The name of the IdField.
        /// </summary>
        private static string IdField { get; } = "ID";

        /// <summary>
        /// The name of the custom field node.
        /// </summary>
        private static string CustomFieldNodeName { get; } = "CustomField";

        /// <summary>
        /// The name of the name field in custom fields.
        /// </summary>
        private static string CustomFieldNameField { get; } = "Name";

        /// <summary>
        /// The name of the value field in custom fields.
        /// </summary>
        private static string CustomFieldValueField { get; } = "Value";

        #endregion

        #region Platform

        /// <summary>
        /// The name of the custom field node.
        /// </summary>
        public static string PlatformNodeName { get; } = "Platform";

        /// <summary>
        /// The name of the custom field node.
        /// </summary>
        public static string PlatformNameField { get; } = "Name";

        #endregion

        #region Game

        /// <summary>
        /// The list of built-in game fields.
        /// </summary>
        private static ImmutableHashSet<string> BuiltInGameFields { get; } = ImmutableHashSet.Create(
            "ApplicationPath", "CommandLine", "Completed", "ConfigurationCommandLine", "ConfigurationPath", "DateAdded", "DateModified", "Developer", "DosBoxConfigurationPath", "Emulator",
            "Favorite", "ID", "ManualPath", "MusicPath", "Notes", "Platform", "Publisher", "Rating", "ReleaseDate", "RootFolder", "ScummVMAspectCorrection", "ScummVMFullscreen",
            "ScummVMGameDataFolderPath", "ScummVMGameType", "ShowBack", "SortTitle", "Source", "StarRating", "Status", "TheGamesDBID", "Title", "UseDosBox", "UseScummVM", "Version",
            "Series", "PlayMode", "Region", "PlayCount", "Portable", "VideoPath", "Hide", "Genre");

        /// <summary>
        /// The sorted list of built-in game fields.
        /// </summary>
        public static ImmutableList<string> SortedBuiltInGameFields { get; } = BuiltInGameFields.OrderBy(f => f).ToImmutableList();

        /// <summary>
        /// The name of the custom field node.
        /// </summary>
        public static string GameNodeName { get; } = "Game";

        /// <summary>
        /// The name of the platform field for games.
        /// </summary>
        public static string GamePlatformField { get; } = "Platform";

        /// <summary>
        /// The name of the GameId field in custom fields.
        /// </summary>
        private static string CustomFieldGameIdField { get; } = "GameID";

        #endregion

        #endregion

        #endregion

        #region File Management Methods

        /// <summary>
        /// Load the given LaunchBox.xml file.
        /// </summary>
        /// <param name="filename">Launchbox directory to load.</param>
        public static void Load(string directory)
        {
            LaunchBoxDirectory = directory;

            // Load the platform list.
            PlatformList = XDocument.Load(Path.Combine(LaunchBoxDirectory, "Data", "Platforms.xml"));
            PlatformListBackup = new XDocument(PlatformList);

            // Load Launchbox Settings file
            LaunchBoxSettings = XDocument.Load(Path.Combine(LaunchBoxDirectory, "Data", "Settings.xml"));

            UpdatePlatforms();

            // Load the games from the platforms.
            GameLists = new Dictionary<string, XDocument>();
            GameListBackups = new Dictionary<string, XDocument>();

            foreach (var platform in Platforms)
            {
                GameLists[platform] = XDocument.Load(Path.Combine(LaunchBoxDirectory, "Data", "Platforms", platform + ".xml"));
                GameListBackups[platform] = new XDocument(GameLists[platform]);
            }
        }

        /// <summary>
        /// Save any changes that have been made.
        /// </summary>
        public static void Save()
        {
            // Load the games from the platforms.
            foreach (var platform in Platforms)
            {
                GameLists[platform].Save(Path.Combine(LaunchBoxDirectory, "Data", "Platforms", platform + ".xml"), SaveOptions.None);
                GameListBackups[platform] = new XDocument(GameLists[platform]);
            }

            // TODO: Support platform renaming.
            //PlatformList.Save(Path.Combine(LaunchBoxDirectory, "Data", "Platforms.xml"), SaveOptions.None);
            //PlatformListBackup = new XDocument(PlatformList);
            // UpdatePlatforms();
        }

        /// <summary>
        /// Undo changes made since the last call to Save or Load.
        /// </summary>
        public static void UndoChanges()
        {
            PlatformList = new XDocument(PlatformList);
            foreach(var platform in Platforms)
                GameLists[platform] = new XDocument(GameListBackups[platform]);
        }

        /// <summary>
        /// Update the platforms shown and selected.
        /// </summary>
        public static void UpdatePlatforms()
        {
            Platforms = PlatformList.Root.Elements(PlatformNodeName).Select(p => GetValue(p, PlatformNameField)).OrderBy(p => p).ToImmutableHashSet();
            SelectedPlatforms = SelectedPlatforms;
        }

        #endregion

        #region XML Wrapping Methods

        /// <summary>
        /// Gets the value of the given element's field.
        /// </summary>
        /// <param name="element">Element containing the field.</param>
        /// <param name="field">Field to get value for.</param>
        /// <returns>Value of the field.</returns>
        public static string GetValue(XElement element, string field)
        {
            var fieldNode = element.Element(field);
            return fieldNode == null ? string.Empty : (fieldNode.Value ?? string.Empty);
        }

        /// <summary>
        /// Sets the value of the given element's field.
        /// </summary>
        /// <param name="element">Element containing the field.</param>
        /// <param name="field">Field to set value for.</param>
        /// <param name="value">New value for the field.</param>
        public static void SetValue(XElement element, string field, string value)
        {
            var fieldNode = element.Element(field);
            if (fieldNode == null)
                element.Add(new XElement(field, value));
            else
                fieldNode.Value = value;
        }

        /// <summary>
        /// Update an elements's value.
        /// </summary>
        /// <param name="element">Element to update.</param>
        /// <param name="field">Name of the field to update.</param>
        /// <param name="value">Value to update.</param>
        /// <param name="replaceExisting">Whether to replace existing field values.</param>
        /// <param name="isTest">Whether this is a test or changes should actually be made.</param>
        /// <param name="testResults">If test is true, results of the test are written to this string.</param>
        public static void UpdateValue(XElement element, string field, string value, bool replaceExisting, bool isTest, StringBuilder testResults)
        {
            // If no change, return.
            var originalValue = GetValue(element, field);
            if (value.Equals(originalValue, StringComparison.InvariantCulture))
                return;

            if (replaceExisting || string.IsNullOrEmpty(originalValue))
            {
                if (isTest)
                    testResults.AppendFormat("{0}: {1} -> {2}\n", field,
                        string.IsNullOrEmpty(originalValue) ? "(empty)" : originalValue,
                        string.IsNullOrEmpty(value) ? "(empty)" : value);
                else
                    SetValue(element, field, value);
            }
        }

        /// <summary>
        /// Gets the value of a custom field for a given item.
        /// </summary>
        /// <param name="document">Document that contains the custom field.</param>
        /// <param name="idFieldName">Name of the id field for the custom field.</param>
        /// <param name="idValue">Id of the item which owns this custom field.</param>
        /// <param name="field">Name of the CustomField to be retrieved.</param>
        /// <returns>Values of the custom field.</returns>
        private static string GetCustomFieldValue(XDocument document, string idFieldName, string idValue, string field)
        {
            var customField = GetCustomField(document, idFieldName, idValue, field);
            if (customField == null)
                return string.Empty;
            return GetValue(customField, CustomFieldValueField);
        }

        /// <summary>
        /// Sets the value of a custom field for a given item.
        /// </summary>
        /// <param name="document">Document that contains the custom field.</param>
        /// <param name="idFieldName">Name of the id field for the custom field.</param>
        /// <param name="idValue">Id of the item which owns this custom field.</param>
        /// <param name="field">Name of the CustomField to be set.</param>
        /// <param name="value">New value for the custom field.</param>
        private static void SetCustomFieldValue(XDocument document, string idFieldName, string idValue, string field, string value)
        {
            var customField = GetCustomField(document, idFieldName, idValue, field);
            if (customField == null)
            {
                document.Root.Add(new XElement(CustomFieldNodeName, new XElement(idFieldName, idValue), new XElement(CustomFieldNameField, field), new XElement(CustomFieldValueField, value)));
            }
            else
                SetValue(customField, CustomFieldValueField, value);
        }

        /// <summary>
        /// Deletes the custom field for a given item.
        /// </summary>
        /// <param name="document">Document that contains the custom field.</param>
        /// <param name="idFieldName">Name of the id field for the custom field.</param>
        /// <param name="idValue">Id of the item which owns this custom field.</param>
        /// <param name="field">Name of the CustomField to be deleted.</param>
        private static void DeleteCustomFieldValue(XDocument document, string idFieldName, string idValue, string field)
        {
            var customField = GetCustomField(document, idFieldName, idValue, field);
            if (customField != null)
                customField.Remove();
        }

        /// <summary>
        /// Gets the custom field for a given item.
        /// </summary>
        /// <param name="document">Document that contains the custom field.</param>
        /// <param name="idFieldName">Name of the id field for the custom field.</param>
        /// <param name="idValue">Id of the item which owns this custom field.</param>
        /// <param name="field">Name of the CustomField to be getting.</param>
        /// <returns>XElement for the requested custom field.</returns>
        private static XElement GetCustomField(XDocument document, string idFieldName, string idValue, string field)
        {
            return document.Root.Elements(CustomFieldNodeName).Where(f => GetValue(f, idFieldName).Equals(idValue, StringComparison.InvariantCulture) && GetValue(f, CustomFieldNameField).Equals(field, StringComparison.InvariantCulture)).FirstOrDefault();
        }

        #endregion

        #region Game Methods

        /// <summary>
        /// Get the games that match the selected platforms.
        /// </summary>
        /// <returns>Games that match the selected platforms.</returns>
        public static IEnumerable<XElement> GetGames()
        {
            return GameLists.Values.SelectMany(document => document.Root.Elements(GameNodeName)).ToList();
        }

        /// <summary>
        /// Get the games that match the selected platforms.
        /// </summary>
        /// <returns>Games that match the selected platforms.</returns>
        public static IEnumerable<XElement> GetGamesBySelectedPlatforms()
        {
            return GameLists.Where(gl => SelectedPlatforms.Contains(gl.Key))
                .SelectMany(gl => gl.Value.Root.Elements(GameNodeName))
                .OrderBy(g => GetGameValue(g, "Platform"))
                .ThenBy(g=> GetGameValue(g, "Title"))
                .ToList();
        }

        /// <summary>
        /// Iterate over the games for the selected platforms.
        /// </summary>
        /// <param name="action">Action to run for each game.</param>
        /// <param name="useSample">Whether to only use a random sample of the games.</param>
        /// <param name="title">Title of the window.</param>
        /// <param name="text">Text to display regarding the operation being completed.</param>
        /// <param name="allowCancel">Whether the user is allowed to cancel the work.</param>
        public static IterateOverGamesResult IterateOverGamesBySelectedPlatforms(Action<XElement> action, bool useSample, string title = "Processing", string text = "Processing games...", bool allowCancel = true)
        {
            IterateOverGamesResult result = IterateOverGamesResult.NeverStarted;

            if (_SelectedPlatforms.Count == 0)
            {
                MessageBox.Show("No platforms selected.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return IterateOverGamesResult.NeverStarted;
            }

            // Show a progress dialog as we iterating through the games.
            ProgressDialog.ShowDialog((dialog) =>
            {
                // Get the games, and a count of the games to be able to show progress.
                var games = GetGamesBySelectedPlatforms();
                if (useSample)
                    games = games.Sample();
                double totalIterations = games.Count(), currentIteration = 0;

                // Iterate over the games.
                foreach (var game in games)
                {
                    // Check for cancelation.
                    if (dialog.CancelRequested)
                    {
                        result = IterateOverGamesResult.Canceled;
                        return;
                    };

                    // Report on the current prgoress.
                    currentIteration++;
                    dialog.ReportProgress((currentIteration / totalIterations) * 100, subText: string.Format("Game {0} of {1}.", currentIteration, totalIterations));

                    try
                    {
                        action(game);
                    }
                    catch
                    {
                        result = IterateOverGamesResult.Error;
                        throw;
                    }
                }

                result = IterateOverGamesResult.Finished;
            }, title, text, allowCancel: allowCancel);

            return result;
        }

        /// <summary>
        /// Get games sorted by platform and title, and then reduced to the number of duplicates.
        /// </summary>
        /// <returns>Number of games with the given platform and title.</returns>
        public static Dictionary<Tuple<string, string>, int> GetGameCounts()
        {
            return (
                from game in GetGamesBySelectedPlatforms()
                group game by Tuple.Create(GetGameValue(game, "Platform"), GetGameValue(game, "Title")) into pairing
                select pairing).ToDictionary(p => p.Key, p => p.Count());
        }

        /// <summary>
        /// Update a game's value.
        /// </summary>
        /// <param name="game">Game to update.</param>
        /// <param name="field">Name of the field to update.</param>
        /// <param name="value">Value to update.</param>
        /// <param name="replaceExisting">Whether to replace existing field values.</param>
        /// <param name="isTest">Whether this is a test or changes should actually be made.</param>
        /// <param name="testResults">If test is true, results of the test are written to this string.</param>
        /// <returns>Whether the value was changed.</returns>
        public static bool UpdateGameValue(XElement game, string field, string value, bool replaceExisting, bool isTest, StringBuilder testResults)
        {
            if (field == GamePlatformField)
                throw new NotImplementedException();

            // If no change, return.
            var originalValue = GetGameValue(game, field);
            if (value.Equals(originalValue, StringComparison.InvariantCulture))
                return false;

            if (replaceExisting || string.IsNullOrEmpty(originalValue))
            {
                if (isTest)
                    testResults.AppendFormat("{0}: {1} -> {2}\n", field,
                        string.IsNullOrEmpty(originalValue) ? "(empty)" : originalValue,
                        string.IsNullOrEmpty(value) ? "(empty)" : value);
                else
                    SetGameValue(game, field, value);

                return true;
            }
            else return false;
        }

        /// <summary>
        /// Get the value for the given game.
        /// </summary>
        /// <param name="game">Game to get field for.</param>
        /// <param name="field">Name of the field to get.</param>
        /// <returns>Value of the field.</returns>
        public static string GetGameValue(XElement game, string field)
        {
            // Handle two special proceduarlly generated fields.
            if (field.Equals("Filename", StringComparison.InvariantCulture))
                return System.IO.Path.GetFileNameWithoutExtension(GetGameValue(game, "ApplicationPath"));
            else if (field.Equals("FilenameTitle", StringComparison.InvariantCulture))
                return NamingConvention.RemoveExtraInfoTags(GetGameValue(game, "Filename"));

            if (BuiltInGameFields.Contains(field))
                return GetValue(game, field);
            else
                return GetCustomFieldValue(game.Document, CustomFieldGameIdField, GetGameValue(game, IdField), field);
        }

        /// <summary>
        /// Get the value for the given game.
        /// </summary>
        /// <param name="game">Game to set field for.</param>
        /// <param name="field">Name of the field to set.</param>
        /// <param name="value">New Value for the field.</param>
        public static void SetGameValue(XElement game, string field, string value)
        {
            if (BuiltInGameFields.Contains(field))
                SetValue(game, field, value);
            else
                SetCustomFieldValue(game.Document, CustomFieldGameIdField, GetGameValue(game, IdField), field, value);
        }

        #endregion

        #region Settings Methods
        /// <summary>
        /// Gets a specified LaunchBox setting from the LaunchBox Settings.xml file.
        /// </summary>
        public static string GetLaunchBoxSetting(string settingName)
        {
            return LaunchBoxSettings.Descendants(settingName).FirstOrDefault().Value;
        }

        /// <summary>
        /// Gets the Folder Path for the given Platform and Media Type.
        /// 
        /// Users can override the default image paths for platforms. This information is stored in the Data/Platforms.xml file in PlatfromFolder nodes.
        /// </summary>
        public static string GetPlatformFolder(string platform, string mediaType)
        {
            XElement platformFolder = (from SettingsXml in PlatformList.Descendants("PlatformFolder")
                                       where SettingsXml.Element("Platform").Value == platform
                                          && SettingsXml.Element("MediaType").Value == mediaType
                                       select SettingsXml).FirstOrDefault();

            return platformFolder != null ? platformFolder.Element("FolderPath").Value : "";
        }
        #endregion

        #region Extension Methods

        /// <summary>
        /// Get a sample of the given enumeration.
        /// </summary>
        /// <param name="elements">Elements to take sample of.</param>
        /// <param name="sampleSize">Number of items to sample.</param>
        /// <returns>A sample of the elements.</returns>
        public static IEnumerable<XElement> Sample(this IEnumerable<XElement> elements, int sampleSize = 25)
        {
            int stepSize = elements.Count() / sampleSize;
            if (stepSize < 1)
                return elements;

            return elements.Where((_, i) => (i % stepSize) == 0);
        }

        /// <summary>
        /// Get a random sample of the given enumeration.
        /// </summary>
        /// <param name="elements">Elements to take sample of.</param>
        /// <param name="sampleSize">Number of items to sample.</param>
        /// <returns>A random sample of the elements.</returns>
        public static IEnumerable<XElement> RandomSample(this IEnumerable<XElement> elements, int sampleSize = 25)
        {
            return elements.OrderBy(p => Random.Next()).Take(sampleSize);
        }

        #endregion

    }
}
