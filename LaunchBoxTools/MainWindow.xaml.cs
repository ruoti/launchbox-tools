﻿using LaunchBoxTools.Dialogs;
using System;
using System.IO;
using System.Windows;
using System.Windows.Documents;

namespace LaunchBoxTools
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public sealed partial class MainWindow : Window
    {

        #region Constructor

        /// <summary>
        /// Create the main window.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        #endregion

        #region Events

        #region LaunchBox

        /// <summary>
        /// Load the initial LaunchBox settings.
        /// </summary>
        private void Window_ContentRendered(object sender, EventArgs e)
        {
            // Load the LaunchBox XML file.
            OpenLaunchBoxXml_Click(null, null);

            // If an XML file was not selected then warn the user.
            if (!LaunchBoxData.IsSetupBindingObject.IsSetup)
                MessageBox.Show("LaunchBox Tools cannot function until a valid LaunchBox.exe file is selected.", "LaunchBox Tools", MessageBoxButton.OK, MessageBoxImage.Error);
            else
                SelectPlatforms_Click(null, null);
        }

        /// <summary>
        /// Open the Launchbox XML file.
        /// </summary>
        private void OpenLaunchBoxXml_Click(object sender, RoutedEventArgs e)
        {
            using (var fileDialog = new System.Windows.Forms.OpenFileDialog
            {
                CheckFileExists = true,
                DefaultExt = "exe",
                Filter = "LaunchBox (LaunchBox.exe)|LaunchBox.exe",
                Multiselect = false,
                Title = "Select the LaunchBox executable",
                ValidateNames = true
            })
            {
                if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    ProgressDialog.ShowDialog(dialog =>
                    {
                        try
                        {
                            LaunchBoxData.Load(Path.GetDirectoryName(fileDialog.FileName));
                            Dispatcher.Invoke(() => LaunchBoxData.IsSetupBindingObject.IsSetup = true);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(string.Format("Invalid LaunchBox directory. Please try again.\n\n{0}", ex.ToString()), "Invalid Settings", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }, "Loading...", "Loading LaunchBox settings...", null, true, true);
            }
        }

        /// <summary>
        /// Select the platforms to operate on.
        /// </summary>
        private void SelectPlatforms_Click(object sender, RoutedEventArgs e)
        {
            PlatformSelectionDialog.ShowDialog();
        }

        #endregion

        #region Scrape

        /// <summary>
        /// Show the filename scraper tool.
        /// </summary>
        private void Scrape_Filename_Click(object sender, RoutedEventArgs e)
        {
            MainContent.Children.Clear();
            MainContent.Children.Add(Scrapers.Filename.Instance);
        }

        /// <summary>
        /// Show the system ini tool.
        /// </summary>
        private void Scrape_SystemIni_Click(object sender, RoutedEventArgs e)
        {
            MainContent.Children.Clear();
            MainContent.Children.Add(Scrapers.SystemIni.Instance);
        }

        /// <summary>
        /// Show the sselph/Scraper tool.
        /// </summary>
        private void Scrape_SselphScraper_Click(object sender, RoutedEventArgs e)
        {
            MainContent.Children.Clear();
            MainContent.Children.Add(Scrapers.SSelphScraper.Instance);
        }

        /// <summary>
        /// Show the synopsis scraper tool.
        /// </summary>
        private void Scrape_SynopsisScraper_Click(object sender, RoutedEventArgs e)
        {
            MainContent.Children.Clear();
            MainContent.Children.Add(Scrapers.SynopsisScraper.Instance);
        }

        #endregion

        #region Rename

        /// <summary>
        /// Show the rename title game tool.
        /// </summary>
        private void Rename_RenameTitle_Click(object sender, RoutedEventArgs e)
        {
            MainContent.Children.Clear();
            MainContent.Children.Add(Rename.TitleRenamer.Instance);
        }

        /// <summary>
        /// Show the rename platform game tool.
        /// </summary>
        private void Rename_RenamePlatforms_Click(object sender, RoutedEventArgs e)
        {
            MainContent.Children.Clear();
            MainContent.Children.Add(Rename.PlatformRenamer.Instance);
        }

        /// <summary>
        /// Show the move files game tool.
        /// </summary>
        private void Rename_MoveFiles_Click(object sender, RoutedEventArgs e)
        {
            MainContent.Children.Clear();
            MainContent.Children.Add(Rename.FileRenamer.Instance);
        }

        #endregion

        #region Hide

        /// <summary>
        /// Show the hide duplicate tool
        /// </summary>
        private void Hide_RemoveDuplicates_Click(object sender, RoutedEventArgs e)
        {
            MainContent.Children.Clear();
            MainContent.Children.Add(LaunchBoxTools.Hide.Duplicates.HideDuplicates.Instance);
        }

        /// <summary>
        /// Show the hide wihtout front box art tool.
        /// </summary>
        private void Hide_HideWithoutFrontBoxArt_Click(object sender, RoutedEventArgs e)
        {
            MainContent.Children.Clear();
            MainContent.Children.Add(LaunchBoxTools.Hide.HideWithoutFront.Instance);
        }

        /// <summary>
        /// Show the unhide tool.
        /// </summary>
        private void Hide_UnhideGames_Click(object sender, RoutedEventArgs e)
        {
            MainContent.Children.Clear();
            MainContent.Children.Add(LaunchBoxTools.Hide.UnhideGames.Instance);
        }

        /// <summary>
        /// Show the hide wihtout front tool.
        /// </summary>
        private void Hide_RemoveHidden_Click(object sender, RoutedEventArgs e)
        {
            MainContent.Children.Clear();
            MainContent.Children.Add(LaunchBoxTools.Hide.RemoveHidden.Instance);
        }

        #endregion

        #region Help

        /// <summary>
        /// Show the scraper tool.
        /// </summary>
        private void Help_About_Click(object sender, RoutedEventArgs e)
        {
            CustomMessageDialog.ShowDialog("Help",
                new Inline[] {
                new Run("LaunchBox Tools is a collection of various tools to help you manage your LaunchBox library in ways currently not supported by LaunchBox."), new LineBreak(), new LineBreak(),

                new Run("Version " + System.Reflection.Assembly.GetEntryAssembly().GetName().Version.ToString()), new LineBreak(),
                CustomMessageDialog.GetClickableHyperlink("https://bitbucket.org/mathflair/launchboxannotator"), new LineBreak(), new LineBreak(),

                new Run("Copyright Scott Ruoti"), new LineBreak(),
                CustomMessageDialog.GetClickableHyperlink("email:mathflair@gmail.com", "mathflair@gmail.com"), new LineBreak(),
                CustomMessageDialog.GetClickableHyperlink("http://isrl.byu.edu/researchers/scott-ruoti/"),
            });
        }

        /// <summary>
        /// Show the scraper tool.
        /// </summary>
        private void Help_Help_Click(object sender, RoutedEventArgs e)
        {
            CustomMessageDialog.ShowDialog("Help",
                new Inline[] {
                new Run("It is recommended that you backup your LaunchBox Data directory before using LaunchBox Tools. " +
                    "While there are not any known bugs that would corrupt this file, it is always better to be safe."), new LineBreak(), new LineBreak(),

                new Run( "Options marked with a (*) will only work if you have a copy of LaunchBox Premium. " +
                    "These features rely on custom fields, and if you don't have LaunchBox Premium, " +
                    "then any custom builds set by LaunchBox Tools will be reset when you run LaunchBox."), new LineBreak(), new LineBreak(),

                new Run("A detailed writeup of all the various features provided by LauchBox Tools can be found at the projects repository ("),
                CustomMessageDialog.GetClickableHyperlink("https://bitbucket.org/mathflair/launchboxannotator"),
                new Run(") or at the LaunchBox forums ("),
                CustomMessageDialog.GetClickableHyperlink("https://www.launchbox-app.com/forum/features/launchboxannotator-tool"),
                new Run(").")
            });
        }

        #endregion

        #endregion

    }
}
