﻿using LaunchBoxTools.Dialogs;
using LaunchBoxTools.NamingConventions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using static LaunchBoxTools.LaunchBoxData;

namespace LaunchBoxTools.Scrapers
{
    /// <summary>
    ///Scraper for sselph's emulation station scraper.
    /// </summary>
    public sealed partial class SSelphScraper : UserControl
    {

        #region Static Properties

        /// <summary>
        /// Singleton for this control.
        /// </summary>
        public static SSelphScraper Instance
        {
            get
            {
                if (instance == null)
                    instance = new SSelphScraper();
                return instance;
            }
        }
        private static SSelphScraper instance = null;

        #endregion

        #region Constructor

        /// <summary>
        /// Create the gamelist scraper control.
        /// </summary>
        private SSelphScraper()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Scrape the filenames.
        /// </summary>
        /// <param name="isTest">Whether this is a test.</param>
        /// <param name="useSample">Whether a sample should be used.</param>
        private void Scrape(bool isTest, bool useSample = false)
        {
            // Get the settings from the UI.
            bool updateTitle = UpdateTitle.IsChecked.Value;
            bool updateMedia = updateTitle && UpdateMedia.IsChecked.Value;
            bool backupTitle = updateTitle && BackupTitle.IsChecked.Value;
            bool updateSortTitle = UpdateSortTitle.IsChecked.Value;

            bool updateTheGamesDBId = UpdateTheGamesDBId.IsChecked.Value;
            bool updateFrontImage = UpdateFrontImage.IsChecked.Value;
            bool updateNotes = UpdateNotes.IsChecked.Value;
            bool updateStarRating = UpdateStarRating.IsChecked.Value;
            bool updateReleaseDate = UpdateReleaseDate.IsChecked.Value;
            bool updateDeveloper = UpdateDeveloper.IsChecked.Value;
            bool updatePublisher = UpdatePublisher.IsChecked.Value;
            bool updateGenre = UpdateGenre.IsChecked.Value;
            bool updatePlayMode = UpdatePlayMode.IsChecked.Value;

            bool updateCustomFields = UpdateCustomFields.IsChecked.Value;
            bool replaceExisting = ReplaceExisting.IsChecked.Value;

            // Load the scraper gamelist and convert paths to absolute paths.
            IEnumerable<XElement> scraperGames = null;
            string scraperDir = null;
            try
            {
                var scraper = XDocument.Load(FileLocation.Text);
                scraperGames = scraper.Root.Elements("game");
                scraperDir = Path.GetDirectoryName(Path.GetFullPath(FileLocation.Text));
                foreach (var path in scraperGames.Select(g => g.Element("path")))
                    path.Value = Path.GetFullPath(Path.Combine(scraperDir, path.Value));
            }
            catch
            {
                MessageBox.Show("Invalid scraper gamelist.xml file. Please try again.", "Invalid File", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // Create necessary objects.
            StringBuilder testResults = isTest ? new StringBuilder() : null;
            var mediaManager = new MediaManager(replaceExisting);

            // Scrape the games.
            var result = IterateOverGamesBySelectedPlatforms((game) =>
            {

                // Match the current game to the scraper list.
                var path = Path.GetFullPath(Path.Combine(LaunchBoxDirectory, GetGameValue(game, "ApplicationPath")));
                var match = scraperGames.Where(sg => sg.Element("path").Value.Equals(path, StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();
                if (match != null)
                {
                    // Add line in the report for this game.
                    if (isTest)
                        testResults.AppendFormat("Game {0}\n", GetGameValue(game, "ApplicationPath"));

                    // Handle the TheGamesDB Id, which is an attribute, not an element.
                    if (updateTheGamesDBId)
                    {
                        var idAttribute = match.Attribute("id");
                        if (idAttribute != null)
                            UpdateGameValue(game, "TheGamesDBID", idAttribute.Value, replaceExisting, isTest, testResults);
                    }

                    // Iterate over the metadata.
                    foreach (var pair in match.Elements())
                    {
                        var key = pair.Name.LocalName;
                        var value = pair.Value.Trim();
                        if (string.IsNullOrEmpty(value))
                            continue;

                        // Handle different fields appropriately.
                        switch (key.ToLowerInvariant())
                        {
                            // Path is already handled and we don't support thubmnail.
                            // TODO: Figure out what thumbnail is and consider adding it.
                            case "path":
                            case "thumbnail":
                                continue;

                            case "name":
                                if (updateSortTitle)
                                    UpdateGameValue(game, "SortTitle", NamingConvention.MoveArticleToBack(value, " -"), replaceExisting, isTest, testResults);

                                if (!updateTitle)
                                    continue;

                                var originalTitle = GetGameValue(game, "Title");
                                if (originalTitle.Equals(value, StringComparison.InvariantCulture))
                                    continue;

                                // Move media if desired.
                                if (updateMedia)
                                    mediaManager.EnqueueTitleRename(game, originalTitle, value);

                                // Backup the original title.
                                if (backupTitle)
                                    UpdateGameValue(game, "OriginalTitle", originalTitle, replaceExisting, isTest, testResults);

                                UpdateGameValue(game, "Title", value, true, isTest, testResults);
                                continue;

                            case "image":
                                if (!updateFrontImage)
                                    continue;
                                mediaManager.EnqueueNewFrontImage(game, Path.GetFullPath(Path.Combine(scraperDir, value)));
                                continue;

                            case "desc":
                                if (!updateNotes)
                                    continue;
                                key = "Notes";
                                break;

                            case "rating":
                                if (!updateStarRating)
                                    continue;
                                key = "StarRating";
                                double starRating;
                                if (!double.TryParse(value, out starRating))
                                    continue;
                                value = ((int)Math.Round(starRating * 5)).ToString();
                                break;

                            case "releasedate":
                                if (!updateReleaseDate)
                                    continue;
                                key = "ReleaseDate";
                                DateTime releaseDate;
                                if (!DateTime.TryParseExact(value, "yyyyMMddThhmmss", CultureInfo.InvariantCulture.DateTimeFormat, DateTimeStyles.AssumeLocal, out releaseDate))
                                    continue;
                                value = releaseDate.ToString("yyyy-MM-ddT00:00:00K", CultureInfo.InvariantCulture.DateTimeFormat);
                                break;

                            case "developer":
                                if (!updateDeveloper)
                                    continue;
                                key = "Developer";
                                break;

                            case "publisher":
                                if (!updatePublisher)
                                    continue;
                                key = "Publisher";
                                break;

                            case "genre":
                                if (!updateGenre)
                                    continue;
                                key = "Genre";
                                break;

                            case "players":
                                if (!updatePlayMode)
                                    continue;
                                key = "PlayMode";
                                break;

                            default:
                                if (!updateCustomFields)
                                    continue;
                                break;
                        }

                        // Handle changes.
                        UpdateGameValue(game, key, value, replaceExisting, isTest, testResults);
                    }

                    if (isTest)
                        testResults.Append("\n");
                }
            }, isTest && useSample);

            // If the operation was canceled or encountered an error, undo any changes that were made.
            if (result != IterateOverGamesResult.Finished)
            {
                UndoChanges();
            }
            // Save or report on changes.
            else
            {
                if (isTest)
                {
                    if (updateMedia)
                        testResults.AppendFormat("\n{0}", mediaManager.ProcessTitleRenames(true));
                    if (updateFrontImage)
                        testResults.AppendFormat("\n{0}", mediaManager.ProcessNewFrontImages(true));
                    Dispatcher.Invoke(() => ScrollableTextDialog.ShowDialog(testResults.ToString(), "Test Results"));
                }
                else
                {
                    if (updateMedia)
                        mediaManager.ProcessTitleRenames();
                    if (updateFrontImage)
                        mediaManager.ProcessNewFrontImages();
                    Save();
                    MessageBox.Show("Finished!", "Finished", MessageBoxButton.OK);
                }
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Select the gamelist file.
        /// </summary>
        private void Select_Click(object sender, RoutedEventArgs e)
        {
            using (var fileDialog = new System.Windows.Forms.OpenFileDialog
            {
                CheckFileExists = true,
                DefaultExt = "xml",
                Filter = "Gamelist (gamelist.xml)|*.xml",
                Multiselect = false,
                Title = "Select gamelist.xml File",
                ValidateNames = true
            })
            {
                if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    FileLocation.Text = fileDialog.FileName;
            }
        }

        /// <summary>
        /// Scrape titles.
        /// </summary>
        private void Scrape_Click(object sender, RoutedEventArgs e)
        {
            Scrape(false);
        }

        /// <summary>
        /// Test scraping titles.
        /// </summary>
        private void Test_All_Click(object sender, RoutedEventArgs e)
        {
            Scrape(true, false);
        }

        /// <summary>
        /// Test scraping titles.
        /// </summary>
        private void Test_Sample_Click(object sender, RoutedEventArgs e)
        {
            Scrape(true, true);
        }

        #endregion

    }
}
