﻿using LaunchBoxTools.Dialogs;
using System;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;

using static LaunchBoxTools.LaunchBoxData;

namespace LaunchBoxTools.Scrapers
{
    /// <summary>
    /// Metadata scraper that relies on filenames.
    /// </summary>
    public sealed partial class Filename : UserControl
    {

        #region Static Properties

        /// <summary>
        /// Singleton for this control.
        /// </summary>
        public static Filename Instance
        {
            get
            {
                if (instance == null)
                    instance = new Filename();
                return instance;
            }
        }
        private static Filename instance = null;

        #endregion

        #region Constructor

        /// <summary>
        /// Create the filename scraper control.
        /// </summary>
        private Filename()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Scrape the filenames.
        /// </summary>
        /// <param name="isTest">Whether this is a test.</param>
        /// <param name="useSample">Whether a sample should be used.</param>
        private void Scrape(bool isTest, bool useSample=false)
        {
            // Get the settings from the UI.
            var namingConvention = Convention.SelectedConvention;
            if (namingConvention == null)
                return;

            bool updateTitle = UpdateTitle.IsChecked.Value;
            bool updateMedia = updateTitle && UpdateMedia.IsChecked.Value;
            bool backupTitle = updateTitle && BackupTitle.IsChecked.Value;
            bool updateSortTitle = UpdateSortTitle.IsChecked.Value;

            bool updateRegion = UpdateRegion.IsChecked.Value;
            bool updateVersion = UpdateVersion.IsChecked.Value;
            bool updateVersionUseDefault = updateVersion && UpdateVersionUseDefault.IsChecked.Value && !string.IsNullOrEmpty(UpdateVersionDefault.Text);
            string defaultVersion = UpdateVersionDefault.Text.Trim();

            bool updateCustomFields = UpdateCustomFields.IsChecked.Value;
            bool replaceExisting = ReplaceExisting.IsChecked.Value;

            // Create necessary objects.
            StringBuilder testResults = isTest ? new StringBuilder() : null;
            var mediaManager = new MediaManager(replaceExisting);

            // Scrape the filename for each game.
            var result = IterateOverGamesBySelectedPlatforms((game) =>
            {
                // Add line in the report for this game.
                if (isTest)
                    testResults.AppendFormat("Game {0}\n", GetGameValue(game, "ApplicationPath"));

                // Parse the filename.
                var values = namingConvention.Parse(GetGameValue(game, "Filename"));

                // Set the default version if needed.
                if (updateVersionUseDefault && !values.ContainsKey("Version"))
                    values["Version"] = defaultVersion;

                // Iterate over the metadata.
                foreach (var pair in values)
                {
                    var key = pair.Key;
                    var value = pair.Value.Trim();
                    if (string.IsNullOrEmpty(value))
                        continue;

                    // Handle different fields appropriately.
                    switch (key)
                    {
                        case "Title":
                            if (!updateTitle)
                                continue;

                            var originalTitle = GetGameValue(game, "Title");
                            if (originalTitle.Equals(value, StringComparison.InvariantCulture))
                                continue;

                            // Move media if desired.
                            if (updateMedia)
                                mediaManager.EnqueueTitleRename(game, originalTitle, value);

                            // Backup the original title.
                            if (backupTitle)
                                UpdateGameValue(game, "OriginalTitle", originalTitle, replaceExisting, isTest, testResults);

                            UpdateGameValue(game, key, value, true, isTest, testResults);
                            continue;

                        case "SortTitle":
                            if (!updateSortTitle)
                                continue;
                            break;

                        case "Region":
                            if (!updateRegion)
                                continue;
                            break;

                        case "Version":
                            if (!updateVersion)
                                continue;
                            break;

                        default:
                            if (!updateCustomFields)
                                continue;
                            break;
                    }

                    // Handle changes.
                    UpdateGameValue(game, key, value, replaceExisting, isTest, testResults);
                }

                if (isTest)
                    testResults.Append("\n");
            }, isTest && useSample);

            // If the operation was canceled or encountered an error, undo any changes that were made.
            if (result != IterateOverGamesResult.Finished)
            {
                UndoChanges();
            }
            // Save or report on changes.
            else
            {
                if (isTest)
                {
                    if (updateMedia)
                        testResults.AppendFormat("\n{0}", mediaManager.ProcessTitleRenames(true));
                    Dispatcher.Invoke(() => ScrollableTextDialog.ShowDialog(testResults.ToString(), "Test Results"));
                }
                else
                {
                    if (updateMedia)
                        mediaManager.ProcessTitleRenames();
                    Save();
                    MessageBox.Show("Finished!", "Finished", MessageBoxButton.OK);
                }
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Scrape titles.
        /// </summary>
        private void Scrape_Click(object sender, RoutedEventArgs e)
        {
            Scrape(false);
        }

        /// <summary>
        /// Test scraping titles.
        /// </summary>
        private void Test_All_Click(object sender, RoutedEventArgs e)
        {
            Scrape(true, false);
        }

        /// <summary>
        /// Test scraping titles.
        /// </summary>
        private void Test_Sample_Click(object sender, RoutedEventArgs e)
        {
            Scrape(true, true);
        }

        #endregion

    }
}
