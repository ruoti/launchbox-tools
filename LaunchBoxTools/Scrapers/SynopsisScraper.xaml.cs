﻿using LaunchBoxTools.Dialogs;
using LaunchBoxTools.NamingConventions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

using static LaunchBoxTools.LaunchBoxData;

namespace LaunchBoxTools.Scrapers
{
    /// <summary>
    /// Scraper for synopsis text files.
    /// </summary>
    public sealed partial class SynopsisScraper : UserControl
    {

        #region Enumerations

        /// <summary>
        /// The states the parser can be in.
        /// </summary>
        private enum ParserState { SearchingForNextSection, ReadingData, ReadingNote };

        #endregion

        #region Static Properties

        /// <summary>
        /// Singleton for this control.
        /// </summary>
        public static SynopsisScraper Instance
        {
            get
            {
                if (instance == null)
                    instance = new SynopsisScraper();
                return instance;
            }
        }
        private static SynopsisScraper instance = null;

        /// <summary>
        /// Regex for matching the start of a section.
        /// </summary>
        private static Regex SectionStartEndRegex { get; } = new Regex(@"^\*$", RegexOptions.Compiled);

        /// <summary>
        /// Regex for matching CRC values.
        /// </summary>
        /// <remarks>Currently I allow this to be slightly wrong. It should be 8 characters, but I allow 7-9 to be more permisive.</remarks>
        private static Regex CRCRegex { get; } = new Regex(@"^([0-9A-Fa-f]{7,9},\s+)*[0-9A-Fa-f]{7,9}$", RegexOptions.Compiled);

        /// <summary>
        /// Regex for matching keys and values in the data.
        /// </summary>
        private static Regex KeyValueRegex { get; } = new Regex(@"^(?<key>[^:]+):\s+(?<value>.*)$", RegexOptions.Compiled);

        /// <summary>
        /// Regex for matching the end of a data section.
        /// </summary>
        private static Regex NoteFollowsRegex { get; } = new Regex("^[_]{25}$", RegexOptions.Compiled);

        #endregion

        #region Constructor

        /// <summary>
        /// Create the synopsis text scraper control.
        /// </summary>
        private SynopsisScraper()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Scrape the filenames.
        /// </summary>
        /// <param name="isTest">Whether this is a test.</param>
        /// <param name="useSample">Whether a sample should be used.</param>
        private void Scrape(bool isTest, bool useSample = false)
        {
            // Get the settings from the UI.
            bool updateTitle = UpdateTitle.IsChecked.Value;
            bool updateMedia = updateTitle && UpdateMedia.IsChecked.Value;
            bool backupTitle = updateTitle && BackupTitle.IsChecked.Value;
            bool updateSortTitle = UpdateSortTitle.IsChecked.Value;

            bool updateNotes = UpdateNotes.IsChecked.Value;
            bool updateRegion = UpdateRegion.IsChecked.Value;
            bool updateReleaseDate = UpdateReleaseDate.IsChecked.Value;
            bool updateDeveloper = UpdateDeveloper.IsChecked.Value;
            bool updatePublisher = UpdatePublisher.IsChecked.Value;
            bool updateGenre = UpdateGenre.IsChecked.Value;
            bool updatePlayMode = UpdatePlayMode.IsChecked.Value;

            bool updateCustomFields = UpdateCustomFields.IsChecked.Value;
            bool replaceExisting = ReplaceExisting.IsChecked.Value;

            // Load the synopsis file.
            var data = ParseSynopsisFile(FileLocation.Text);
            if (data == null)
                return;

            // Create necessary objects.
            StringBuilder testResults = isTest ? new StringBuilder() : null;
            var mediaManager = new MediaManager(replaceExisting);

            // Scrape the games.
            var result = IterateOverGamesBySelectedPlatforms((game) =>
            {
                // Match the game against the System INI file.
                // First, try matching against the title.
                var title = GetGameValue(game, "Title");

                // Next, try matching against the title in the filename.
                if (!data.ContainsKey(title))
                    title = GetGameValue(game, "FilenameTitle");

                // Next, try matching against the full filename.
                if (!data.ContainsKey(title))
                    title = GetGameValue(game, "Filename");

                // Finally, try parsing the filename using the No-Intro naming convention, and using the parsed title.
                if (!data.ContainsKey(title))
                {
                    var attemptedMatch = NoIntro_2007_10_30.Instance.Parse(GetGameValue(game, "Filename"));
                    if (attemptedMatch.ContainsKey("Title"))
                        title = attemptedMatch["Title"];
                }

                if (data.ContainsKey(title))
                {
                    // Get the matching item.
                    var match = data[title];

                    // Add line in the report for this game.
                    if (isTest)
                        testResults.AppendFormat("Game {0}\n", GetGameValue(game, "ApplicationPath"));

                    // Handle the title, as it is the section header and not a value.
                    if (updateSortTitle)
                        UpdateGameValue(game, "SortTitle", NamingConvention.MoveArticleToBack(title, " -"), replaceExisting, isTest, testResults);

                    if (updateTitle)
                    {
                        var originalTitle = GetGameValue(game, "Title");
                        if (!originalTitle.Equals(title, StringComparison.InvariantCulture))
                        {
                            // Move media if desired.
                            if (updateMedia)
                                mediaManager.EnqueueTitleRename(game, originalTitle, title);

                            // Backup the original title.
                            if (backupTitle)
                                UpdateGameValue(game, "OriginalTitle", originalTitle, replaceExisting, isTest, testResults);

                            UpdateGameValue(game, "Title", title, true, isTest, testResults);
                        }
                    }

                    // Iterate over the metadata.
                    foreach (var pair in match)
                    {
                        var key = pair.Key;
                        var value = pair.Value.Trim();
                        if (string.IsNullOrEmpty(value))
                            continue;

                        // Handle different fields appropriately.
                        switch (key)
                        {
                            // Renaming platforms is a can of worms. Ignore this field.
                            case "Platform":
                                continue;

                            case "Notes":
                                if (!updateNotes)
                                    continue;
                                break;

                            case "Region":
                                if (!updateRegion)
                                    continue;
                                break;

                            case "Release Year":
                                if (!updateReleaseDate)
                                    continue;
                                key = "ReleaseDate";
                                DateTime releaseDate;
                                if (!DateTime.TryParseExact(value, new[] { "MMM dd, yyyy", "MMM, yyyy", "yyyy" }, CultureInfo.InvariantCulture.DateTimeFormat, DateTimeStyles.AssumeLocal, out releaseDate))
                                    continue;
                                value = releaseDate.ToString("yyyy-MM-ddT00:00:00K", CultureInfo.InvariantCulture.DateTimeFormat);
                                break;

                            case "Developer":
                                if (!updateDeveloper)
                                    continue;
                                break;

                            case "Publisher":
                                if (!updatePublisher)
                                    continue;
                                break;

                            case "Genre":
                                if (!updateGenre)
                                    continue;
                                break;

                            case "Players":
                                if (!updatePlayMode)
                                    continue;
                                key = "PlayMode";
                                break;

                            default:
                                if (!updateCustomFields)
                                    continue;
                                break;
                        }

                        // Handle changes.
                        UpdateGameValue(game, key, value, replaceExisting, isTest, testResults);
                    }

                    if (isTest)
                        testResults.Append("\n");
                }
            }, isTest && useSample);

            // If the operation was canceled or encountered an error, undo any changes that were made.
            if (result != IterateOverGamesResult.Finished)
            {
                UndoChanges();
            }
            // Save or report on changes.
            else
            {
                if (isTest)
                {
                    if (updateMedia)
                        testResults.AppendFormat("\n{0}", mediaManager.ProcessTitleRenames(true));
                    Dispatcher.Invoke(() => ScrollableTextDialog.ShowDialog(testResults.ToString(), "Test Results"));
                }
                else
                {
                    if (updateMedia)
                        mediaManager.ProcessTitleRenames();
                    Save();
                    MessageBox.Show("Finished!", "Finished", MessageBoxButton.OK);
                }
            }
        }

        /// <summary>
        /// Parse the synopsis file.
        /// </summary>
        /// <param name="filename">Synopsis file to parse.</param>
        private static Dictionary<string, Dictionary<string, string>> ParseSynopsisFile(string filename)
        {
            var data = new Dictionary<string, Dictionary<string, string>>(StringComparer.InvariantCultureIgnoreCase);

            try
            {
                string line;
                var state = ParserState.SearchingForNextSection;
                Dictionary<string, string> currentSet = null;
                StringBuilder note = null;

                // Read the file line by line.
                using (var file = new StreamReader(filename))
                {
                    var lineNumber = 1;
                    string title = null;
                    string crc = null;

                    while ((line = file.ReadLine()) != null)
                    {
                        lineNumber++;
                        line = System.Web.HttpUtility.HtmlDecode(line.Trim());

                        switch (state)
                        {
                            case ParserState.SearchingForNextSection:
                                if (SectionStartEndRegex.IsMatch(line))
                                {
                                    // Found the start of data. Read the title and possibly the crc.
                                    state = ParserState.ReadingData;
                                    currentSet = new Dictionary<string, string>();

                                    crc = System.Web.HttpUtility.HtmlDecode(file.ReadLine().Trim());
                                    title = System.Web.HttpUtility.HtmlDecode(file.ReadLine().Trim());
                                    lineNumber += 2;


                                    data[title] = currentSet; // Title
                                    if (!string.IsNullOrEmpty(crc))
                                        currentSet["CRC"] = crc;
                                }
                                continue;

                            case ParserState.ReadingData:
                                if (NoteFollowsRegex.IsMatch(line))
                                {
                                    state = ParserState.ReadingNote;
                                    note = new StringBuilder();
                                    file.ReadLine(); // Eat a line.
                                    lineNumber++;
                                }
                                else if (SectionStartEndRegex.IsMatch(line))
                                {
                                    state = ParserState.SearchingForNextSection;
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(line))
                                        continue;

                                    var keyValuePairMatch = KeyValueRegex.Match(line);
                                    if (!keyValuePairMatch.Success)
                                        throw new InvalidOperationException(string.Format("Key value pair not found.\n\nLine number: {0}\nTitle: {1}\nValue: {2}.",
                                            lineNumber, title, line));
                                    currentSet[keyValuePairMatch.Groups["key"].Value] = keyValuePairMatch.Groups["value"].Value;
                                }
                                continue;

                            case ParserState.ReadingNote:
                                if (SectionStartEndRegex.IsMatch(line))
                                {
                                    state = ParserState.SearchingForNextSection;
                                    currentSet["Notes"] = note.ToString();
                                }
                                else
                                {
                                    if (note.Length != 0)
                                        note.Append("\n");
                                    note.Append(line);
                                }
                                continue;
                        }

                    }
                }

                // We should end searching for data.
                if (state != ParserState.SearchingForNextSection)
                    throw new InvalidOperationException("A game's synopsis info was not correctly closed.");

                return data;
            }
            catch (Exception e)
            {
                MessageBox.Show(string.Format("Error loading synopsis file! Ensure you have a valid file and try again.\n\n{0}", e.ToString()), "Error Loading", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Select the gamelist file.
        /// </summary>
        private void Select_Click(object sender, RoutedEventArgs e)
        {
            using (var fileDialog = new System.Windows.Forms.OpenFileDialog
            {
                CheckFileExists = true,
                DefaultExt = "txt",
                Filter = "Synopsis (*.txt)|*.txt",
                Multiselect = false,
                Title = "Select Synopsis File",
                ValidateNames = true
            })
            {
                if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    FileLocation.Text = fileDialog.FileName;
            }
        }

        /// <summary>
        /// Scrape titles.
        /// </summary>
        private void Scrape_Click(object sender, RoutedEventArgs e)
        {
            Scrape(false);
        }

        /// <summary>
        /// Test scraping titles.
        /// </summary>
        private void Test_All_Click(object sender, RoutedEventArgs e)
        {
            Scrape(true, false);
        }

        /// <summary>
        /// Test scraping titles.
        /// </summary>
        private void Test_Sample_Click(object sender, RoutedEventArgs e)
        {
            Scrape(true, true);
        }

        #endregion

    }
}
