﻿using IniParser;
using IniParser.Model;
using LaunchBoxTools.Dialogs;
using LaunchBoxTools.NamingConventions;
using System;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

using static LaunchBoxTools.LaunchBoxData;

namespace LaunchBoxTools.Scrapers
{
    /// <summary>
    /// Scraper for System INI files.
    /// </summary>
    public sealed partial class SystemIni : UserControl
    {

        #region Static Properties

        /// <summary>
        /// Singleton for this control.
        /// </summary>
        public static SystemIni Instance
        {
            get
            {
                if (instance == null)
                    instance = new SystemIni();
                return instance;
            }
        }
        private static SystemIni instance = null;

        /// <summary>
        /// Regex to match ini section headers.
        /// </summary>
        private static Regex IniSectionHeaderRegex { get; } = new Regex(@"^\[[^\]]+]$", RegexOptions.Compiled);

        #endregion

        #region Constructor

        /// <summary>
        /// Create the System INI scraper control.
        /// </summary>
        private SystemIni()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Scrape the filenames.
        /// </summary>
        /// <param name="isTest">Whether this is a test.</param>
        /// <param name="useSample">Whether a sample should be used.</param>
        private void Scrape(bool isTest, bool useSample = false)
        {
            // Get the settings from the UI.
            bool updateTitle = UpdateTitle.IsChecked.Value;
            bool updateMedia = updateTitle && UpdateMedia.IsChecked.Value;
            bool backupTitle = updateTitle && BackupTitle.IsChecked.Value;
            bool updateSortTitle = UpdateSortTitle.IsChecked.Value;

            bool updateNotes = UpdateNotes.IsChecked.Value;
            bool updateStarRating = UpdateStarRating.IsChecked.Value;
            bool updateRating = UpdateRating.IsChecked.Value;
            bool updateReleaseDate = UpdateReleaseDate.IsChecked.Value;
            bool updateDeveloper = UpdateDeveloper.IsChecked.Value;
            bool updatePublisher = UpdatePublisher.IsChecked.Value;
            bool updateGenre = UpdateGenre.IsChecked.Value;
            bool updatePlayMode = UpdatePlayMode.IsChecked.Value;

            bool updateCustomFields = UpdateCustomFields.IsChecked.Value;
            bool replaceExisting = ReplaceExisting.IsChecked.Value;

            // Load the system INI.
            IniData ini = null;
            try
            {
                var parser = new FileIniDataParser();
                parser.Parser.Configuration.SectionRegex = IniSectionHeaderRegex;
                ini = new IniDataCaseInsensitive(parser.ReadFile(FileLocation.Text));
            }
            catch (Exception e)
            {
                MessageBox.Show(string.Format("Error loading System INI file! Ensure you have a valid file and try again.\n\n{0}", e.ToString()),
                    "Error Loading", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // Create necessary objects.
            StringBuilder testResults = isTest ? new StringBuilder() : null;
            var mediaManager = new MediaManager(replaceExisting);

            // Scrape the games.
            var result = IterateOverGamesBySelectedPlatforms((game) =>
            {
                // Match the game against the System INI file.
                // First, try matching against the title.
                var title = GetGameValue(game, "Title");
                var match = ini[title];

                // Next, try matching against the title in the filename.
                if (match == null)
                {
                    title = GetGameValue(game, "FilenameTitle");
                    match = ini[title];
                }

                // Next, try matching against the full filename.
                if (match == null)
                {
                    title = GetGameValue(game, "Filename");
                    match = ini[title];
                }

                // Finally, try parsing the filename using the No-Intro naming convention, and using the parsed title.
                if (match == null)
                {
                    var attemptedMatch = NoIntro_2007_10_30.Instance.Parse(GetGameValue(game, "Filename"));
                    if (attemptedMatch.ContainsKey("Title"))
                    {
                        title = attemptedMatch["Title"];
                        match = ini[title];
                    }
                }

                if (match != null)
                {
                    // Add line in the report for this game.
                    if (isTest)
                        testResults.AppendFormat("Game {0}\n", GetGameValue(game, "ApplicationPath"));

                    // Handle the title, as it is the section header and not a value.
                    if (updateSortTitle)
                        UpdateGameValue(game, "SortTitle", NamingConvention.MoveArticleToBack(title, " -"), replaceExisting, isTest, testResults);

                    if (updateTitle)
                    {
                        var originalTitle = GetGameValue(game, "Title");
                        if (!originalTitle.Equals(title, StringComparison.InvariantCulture))
                        {
                            // Move media if desired.
                            if (updateMedia)
                                mediaManager.EnqueueTitleRename(game, originalTitle, title);

                            // Backup the original title.
                            if (backupTitle)
                                UpdateGameValue(game, "OriginalTitle", originalTitle, replaceExisting, isTest, testResults);

                            UpdateGameValue(game, "Title", title, true, isTest, testResults);
                        }
                    }

                    // Iterate over the metadata.
                    foreach (var pair in match)
                    {
                        var key = pair.KeyName;
                        var value = pair.Value.Trim();
                        if (string.IsNullOrEmpty(value))
                            continue;

                        // Handle different fields appropriately.
                        switch (key.ToLowerInvariant())
                        {
                            case "description":
                                if (!updateNotes)
                                    continue;
                                key = "Notes";
                                break;

                            case "score":
                                if (!updateStarRating)
                                    continue;
                                key = "StarRating";
                                double starRating;
                                if (!double.TryParse(value, out starRating))
                                    continue;
                                value = ((int)Math.Round(starRating * 5)).ToString();
                                break;

                            case "released":
                                if (!updateReleaseDate)
                                    continue;
                                key = "ReleaseDate";
                                DateTime releaseDate;
                                if (!DateTime.TryParseExact(value, new[] { "MMM dd, yyyy", "MMM, yyyy", "yyyy" }, CultureInfo.InvariantCulture.DateTimeFormat, DateTimeStyles.AssumeLocal, out releaseDate))
                                    continue;
                                value = releaseDate.ToString("yyyy-MM-ddT00:00:00K", CultureInfo.InvariantCulture.DateTimeFormat);
                                break;

                            case "esrb":
                                if (!updateRating)
                                    break;
                                key = "Rating";
                                break;

                            case "developer":
                                if (!updateDeveloper)
                                    continue;
                                key = "Developer";
                                break;

                            case "publisher":
                                if (!updatePublisher)
                                    continue;
                                key = "Publisher";
                                break;

                            case "genre":
                                if (!updateGenre)
                                    continue;
                                key = "Genre";
                                break;

                            case "players":
                                if (!updatePlayMode)
                                    continue;
                                key = "PlayMode";
                                break;

                            default:
                                if (!updateCustomFields)
                                    continue;
                                break;
                        }

                        // Handle changes.
                        UpdateGameValue(game, key, value, replaceExisting, isTest, testResults);
                    }

                    if (isTest)
                        testResults.Append("\n");
                }
            }, isTest && useSample);

            // If the operation was canceled or encountered an error, undo any changes that were made.
            if (result != IterateOverGamesResult.Finished)
            {
                UndoChanges();
            }
            // Save or report on changes.
            else
            {
                if (isTest)
                {
                    if (updateMedia)
                        testResults.AppendFormat("\n{0}", mediaManager.ProcessTitleRenames(true));
                    Dispatcher.Invoke(() => ScrollableTextDialog.ShowDialog(testResults.ToString(), "Test Results"));
                }
                else
                {
                    if (updateMedia)
                        mediaManager.ProcessTitleRenames();
                    Save();
                    MessageBox.Show("Finished!", "Finished", MessageBoxButton.OK);
                }
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Select the gamelist file.
        /// </summary>
        private void Select_Click(object sender, RoutedEventArgs e)
        {
            using (var fileDialog = new System.Windows.Forms.OpenFileDialog
            {
                CheckFileExists = true,
                DefaultExt = "xml",
                Filter = "System INI (*.ini)|*.ini",
                Multiselect = false,
                Title = "Select System INI File",
                ValidateNames = true
            })
            {
                if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    FileLocation.Text = fileDialog.FileName;
            }
        }

        /// <summary>
        /// Scrape titles.
        /// </summary>
        private void Scrape_Click(object sender, RoutedEventArgs e)
        {
            Scrape(false);
        }

        /// <summary>
        /// Test scraping titles.
        /// </summary>
        private void Test_All_Click(object sender, RoutedEventArgs e)
        {
            Scrape(true, false);
        }

        /// <summary>
        /// Test scraping titles.
        /// </summary>
        private void Test_Sample_Click(object sender, RoutedEventArgs e)
        {
            Scrape(true, true);
        }

        #endregion

    }
}
