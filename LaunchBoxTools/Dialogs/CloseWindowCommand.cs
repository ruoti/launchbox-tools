﻿using System;
using System.Windows;
using System.Windows.Input;

namespace LaunchBoxTools.Dialogs
{

    /// <summary>
    /// Command to close the current window.
    /// </summary>
    public sealed class CloseWindowCommand : ICommand
    {

        #region Properties

        /// <summary>
        /// The can execute event.
        /// </summary>
        public event EventHandler CanExecuteChanged;

        /// <summary>
        /// Close window command instance.
        /// </summary>
        public static ICommand Instance { get; } = new CloseWindowCommand();

        #endregion

        #region Constructor

        /// <summary>
        /// Create the close window command.
        /// </summary>
        private CloseWindowCommand() { }

        #endregion

        #region ICommand Methods

        /// <summary>
        /// Only works for windows.
        /// </summary>
        /// <param name="parameter">Parameter that the command is applied to.</param>
        /// <returns>Whether the parameter is a window.</returns>
        public bool CanExecute(object parameter)
        {
            return parameter is Window;
        }

        /// <summary>
        /// Execute the close command.
        /// </summary>
        /// <param name="parameter">Parameter that the command is applied to.</param>
        public void Execute(object parameter)
        {
            if (CanExecute(parameter))
                (parameter as Window).Close();
        }

        #endregion

    }
}
