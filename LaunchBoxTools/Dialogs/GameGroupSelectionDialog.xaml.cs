﻿using LaunchBoxTools.Hide.Duplicates;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace LaunchBoxTools.Dialogs
{
    /// <summary>
    /// Window to select from groupings of games.
    /// </summary>
    public sealed partial class GameGroupSelectionDialog : Window
    {

        #region Property

        /// <summary>
        /// The items displayed in this form.
        /// </summary>
        private ObservableCollection<GameGroup> Items { get; }

        /// <summary>
        /// Did the operation complete.
        /// </summary>
        private bool? Completed { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Create the window for selecting game groupings.
        /// </summary>
        /// <param name="groupings">Groups that selections are made from.</param>
        public GameGroupSelectionDialog(IEnumerable<GameGroup> gameGroups)
        {
            InitializeComponent();

            Completed = null;

            Items = new ObservableCollection<GameGroup>(gameGroups);
            GameGroups.ItemsSource = Items;
            HideCompleted_Click(null, null);
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// Show the game group selection dialog.
        /// </summary>
        /// <param name="gameGroups">Groups that selections are made from.</param>
        /// <returns>Game grouping selections.</returns>
        public static IEnumerable<GameGroup> ShowDialog(IEnumerable<GameGroup> gameGroups)
        {
            var dialog = new GameGroupSelectionDialog(gameGroups);
            dialog.ShowDialog();
            if (dialog.Completed.HasValue && dialog.Completed.Value)
                return dialog.Items.ToImmutableList();
            else
                return null;
        }

        #endregion

        #region Events

        /// <summary>
        /// Hide game groups that have an item selected.
        /// </summary>
        private void HideCompleted_Click(object sender, RoutedEventArgs e)
        {
            GameGroups.Items.Filter = item => !((GameGroup)item).IsItemSelected;
        }

        /// <summary>
        /// Show all game groups.
        /// </summary>
        private void ShowCompleted_Click(object sender, RoutedEventArgs e)
        {
            GameGroups.Items.Filter = null;
        }

        /// <summary>
        /// Ignored all input into text fields.
        /// </summary>
        /// <remarks>Text fields are editable to allow carot controls, but we don't want to actually allow text input.</remarks>
        private void DataGrid_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = true;
        }

        /// <summary>
        /// Ignore copy, paste, and context menu command into text fiels.
        /// </summary>
        /// <remarks>Text fields are editable to allow carot controls, but we don't want to actually allow text input.</remarks>
        private void DataGrid_PreviewCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if ((e.Command == ApplicationCommands.Cut) || (e.Command == ApplicationCommands.Paste))
            {
                e.Handled = true;
                e.CanExecute = false;
            }
        }

        /// <summary>
        /// Send the mouse wheel scroll event from the child datagrid to the parent items control.
        /// </summary>
        /// <remarks>This fixes a poor implementation choice by Microsoft. Even when the child DataGrid doesn't need to
        /// scroll, it still eats all mouse wheel events.</remarks>
        private void DataGrid_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            e.Handled = true;

            var eventArg = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
            eventArg.RoutedEvent = UIElement.MouseWheelEvent;
            eventArg.Source = sender;

            var parent = ((Control)sender).Parent as UIElement;
            parent.RaiseEvent(eventArg);
        }

        /// <summary>
        /// Clear all selection choices.
        /// </summary>
        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            // Find the parent data grid.
            var element = (FrameworkElement)sender;
            while (element != null & !(element is DataGrid))
                element = VisualTreeHelper.GetParent(element) as FrameworkElement;
            
            if (element != null)
            {
                var gameGroup = (GameGroup)element.DataContext;
                foreach (var gameItem in gameGroup.Items)
                    gameItem.IsSelected = false;
            }
        }

        /// <summary>
        /// Fired when the radio button is checked.
        /// </summary>
        /// <remarks>Fixes a problem with Microsoft's implementaiton of two-way databinding in named groups.</remarks>
        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            var gameItem = (GameGroupItem)((RadioButton)sender).DataContext;
            gameItem.IsSelected = true;
        }

        /// <summary>
        /// Fired when the radio button is unchecked.
        /// </summary>
        /// <remarks>Fixes a problem with Microsoft's implementaiton of two-way databinding in named groups.</remarks>
        private void RadioButton_Unchecked(object sender, RoutedEventArgs e)
        {
            var gameItem = (GameGroupItem)((RadioButton)sender).DataContext;
            gameItem.IsSelected = false;
        }

        /// <summary>
        /// Display the full XML for the game.
        /// </summary>
        private void Show_XML_Clicked(object sender, RoutedEventArgs e)
        {
            var gameItem = (GameGroupItem)((Button)sender).DataContext;
            ScrollableTextDialog.Show(gameItem.FullXML, "Game XML", true);
        }

        /// <summary>
        /// Save choices.
        /// </summary>
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            if (Items.Any(i => !i.IsItemSelected))
            {
                if (MessageBox.Show("Some games groups do not have a game selected. Are you sure you wish to continue?", "Continue?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                    return;
            }

            Completed = true;
            Close();
        }

        /// <summary>
        /// Cancel choices.
        /// </summary>
        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Completed = false;
            Close();
        }

        /// <summary>
        /// Ensures the user knows what closing this window will do.
        /// </summary>
        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (!Completed.HasValue)
            {
                if (MessageBox.Show("Cancel game selection?\n\nThis will cancel the entire hide duplicate operation. If you want to save the portion you have already done, please click save.",
                    "Cancel?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes) == MessageBoxResult.Yes)
                    Completed = false;
                else
                    e.Cancel = true;
            }
        }

        #endregion

    }
}
