﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

namespace LaunchBoxTools.Dialogs
{
    /// <summary>
    /// Dialog used to select the platforms to operate on.
    /// </summary>
    public sealed partial class PlatformSelectionDialog : Window
    {

        #region PlatformsItem Inner Class

        /// <summary>
        /// Inner class for PlatformItem.
        /// </summary>
        private sealed class PlatformItem : DependencyObject
        {
            /// <summary>
            /// Get the platform name.
            /// </summary>
            public string Platform { get; }

            /// <summary>
            /// Selected dependency property.
            /// </summary>
            public static readonly DependencyProperty IsSelectedProperty =
                DependencyProperty.Register("IsSelected", typeof(bool), typeof(PlatformItem));

            /// <summary>
            /// Get or set whether the platform is selected.
            /// </summary>
            public bool IsSelected
            {
                get { return (bool)GetValue(IsSelectedProperty); }
                set { SetValue(IsSelectedProperty, value); }
            }

            /// <summary>
            /// Construct the platform item.
            /// </summary>
            /// <param name="platform">Name of the platform.</param>
            /// <param name="selected">Initial selection state.</param>
            public PlatformItem(string platform, bool selected)
            {
                Platform = platform;
                IsSelected = selected;
            }

        }

        #endregion

        #region Property

        /// <summary>
        /// The items displayed in this form.
        /// </summary>
        private ObservableCollection<PlatformItem> Items { get; }

        #endregion

        #region Constructor

        /// <summary>
        /// Create the platform selection dialog.
        /// </summary>
        private PlatformSelectionDialog()
        {
            InitializeComponent();

            Items = new ObservableCollection<PlatformItem>(
                from platform in LaunchBoxData.Platforms
                orderby platform
                select new PlatformItem(platform, LaunchBoxData.SelectedPlatforms.Contains(platform)));
            Platforms.ItemsSource = Items;
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// Show the select platforms dialog.
        /// </summary>
        public static new void ShowDialog()
        {
            ((Window)new PlatformSelectionDialog()).ShowDialog();
        }

        #endregion

        #region Events

        /// <summary>
        /// Select all the items.
        /// </summary>
        private void SelectAll_Click(object sender, RoutedEventArgs e)
        {
            foreach (var item in Items)
                item.IsSelected = true;
        }

        /// <summary>
        /// Select none of the items.
        /// </summary>
        private void SelectNone_Click(object sender, RoutedEventArgs e)
        {
            foreach (var item in Items)
                item.IsSelected = false;
        }

        /// <summary>
        /// Save the platform choices.
        /// </summary>
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            LaunchBoxData.SelectedPlatforms = Items.Where(p => p.IsSelected).Select(p => p.Platform);
            Close();
        }

        #endregion

    }
}
