﻿using System;
using System.ComponentModel;
using System.Text;
using System.Windows;

namespace LaunchBoxTools.Dialogs
{
    /// <summary>
    /// Dialog to show progress for a long running operation.
    /// </summary>
    public sealed partial class ProgressDialog : Window, IDisposable
    {

        #region Dependency Properties

        /// <summary>
        /// Property for Text.
        /// </summary>
        private static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(ProgressDialog), new UIPropertyMetadata("Operation in Progress..."));

        /// <summary>
        /// Get or set the main text for this dialog.
        /// </summary>
        private string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, (value ?? string.Empty).Trim()); }
        }

        /// <summary>
        /// Property for SubText.
        /// </summary>
        private static readonly DependencyProperty SubTextProperty =
            DependencyProperty.Register("SubText", typeof(string), typeof(ProgressDialog), new UIPropertyMetadata("Test"));

        /// <summary>
        /// Get or set the sub text for this dialog.
        /// </summary>
        private string SubText
        {
            get { return (string)GetValue(SubTextProperty); }
            set { SetValue(SubTextProperty, (value ?? string.Empty).Trim()); }
        }

        /// <summary>
        /// Property for TimeRemaining.
        /// </summary>
        private static readonly DependencyProperty TimeRemainingProperty =
            DependencyProperty.Register("TimeRemaining", typeof(string), typeof(ProgressDialog), new UIPropertyMetadata(string.Empty));

        /// <summary>
        /// Get or set the time remaining for the operation.
        /// </summary>
        private string TimeRemaining
        {
            get { return (string)GetValue(TimeRemainingProperty); }
            set { SetValue(TimeRemainingProperty, value); }
        }

        /// <summary>
        /// Property for IsIndeterminate.
        /// </summary>
        private static readonly DependencyProperty IsIndeterminateProperty =
            DependencyProperty.Register("IsIndeterminate", typeof(bool), typeof(ProgressDialog), new UIPropertyMetadata(false));

        /// <summary>
        /// Get or set whether the operation is indeterminate in terms of progress.
        /// </summary>
        private bool IsIndeterminate
        {
            get { return (bool)GetValue(IsIndeterminateProperty); }
            set { SetValue(IsIndeterminateProperty, value); }
        }

        /// <summary>
        /// Property for AllowCancel.
        /// </summary>
        private static readonly DependencyProperty AllowCancelProperty =
            DependencyProperty.Register("AllowCancel", typeof(bool), typeof(ProgressDialog), new UIPropertyMetadata(false));

        /// <summary>
        /// Get or set whether the operation is allowed to be canceled.
        /// </summary>
        private bool AllowCancel
        {
            get { return (bool)GetValue(AllowCancelProperty); }
            set { SetValue(AllowCancelProperty, value); }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Get the worker that wraps the work.
        /// </summary>
        private readonly BackgroundWorker Worker;

        /// <summary>
        /// Time the background worker started.
        /// </summary>
        private DateTime StartTime { get; set; }

        /// <summary>
        /// Get whether their is a request for the operation to be canceled.
        /// </summary>
        public bool CancelRequested { get; private set; } = false;

        /// <summary>
        /// Get or set whether the operation is finished.
        /// </summary>
        private bool IsDone { get; set; } = false;

        #endregion

        #region Constructor

        /// <summary>
        /// Create the pgoress dialog.
        /// </summary>
        /// <param name="work">Operation that will be executed in the background. Gets a reference to the progress dialog to check if a cancelation has been requested or to update progress.</param>
        private ProgressDialog(Action<ProgressDialog> work)
        {
            InitializeComponent();

            Worker = new BackgroundWorker();
            Worker.WorkerReportsProgress = false;
            Worker.DoWork += (_, __) =>
            {
                try
                {
                    work(this);
                }
                catch (Exception e)
                {
                    Dispatcher.Invoke(() => ScrollableTextDialog.ShowDialog(string.Format("Unexpected error completing the operation.\n\n{0}", e.ToString()), "Unexpected Error"));
                }
                finally
                {
                    IsDone = true;
                    Dispatcher.Invoke(() => Close());
                }
            };
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// Show a progress dialog while work is being completed.
        /// </summary>
        /// <param name="work">Operation that will be executed in the background. Gets a reference to the progress dialog to check if a cancelation has been requested or to update progress.</param>
        /// <param name="title">Title of the window.</param>
        /// <param name="text">Text to display regarding the operation being completed.</param>
        /// <param name="subText">Subtext to display regarding the operation being completed.</param>
        /// <param name="isIndeterminate">Whether a percent completed is able to be calculated.</param>
        /// <param name="allowCancel">Whether the user is allowed to cancel the work.</param>
        public static void ShowDialog(Action<ProgressDialog> work, string title, string text, string subText = null, bool isIndeterminate = false, bool allowCancel = false)
        {
            var dialog = new ProgressDialog(work)
            {
                Title = (title ?? string.Empty).Trim(),
                Text = text,
                SubText = subText,
                IsIndeterminate = isIndeterminate,
                TimeRemaining = isIndeterminate ? string.Empty : "Calculating time remaining...",
                AllowCancel = allowCancel
            };
            dialog.ShowDialog();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Report on the progress of the operation.
        /// </summary>
        /// <param name="percentage">Percentage of the work that has been completed (0-100). Ignored if progress is indeterminate.</param>
        /// <param name="text">New text to display in the dialog.</param>
        /// <param name="subText">New subtext to display in the dialog.</param>
        public void ReportProgress(double? percentage = null, string text = null, string subText = null)
        {
            var timeElapsed = DateTime.Now - StartTime;
            var millisecondsEllapsed = timeElapsed.TotalMilliseconds;
            var totalTime = (100.0 / percentage) * millisecondsEllapsed;
            var remainingTime = totalTime - millisecondsEllapsed;

            Dispatcher.Invoke(() =>
            {
                if (text != null)
                    Text = text;
                if (subText != null)
                    SubText = subText;
                if (!IsIndeterminate && percentage.HasValue)
                {
                    // Calculate time remaining and format it in a human readable format.
                    var value = percentage.Value;
                    var timeRemaining = TimeSpan.FromSeconds(Math.Ceiling(((100 - value) / value) * (DateTime.Now - StartTime).TotalSeconds));
                    var timeString = new StringBuilder("About ");
                    if (timeRemaining.Days > 0)
                        timeString.AppendFormat("{0} day{1} ", timeRemaining.Days, timeRemaining.Days > 1 ? "s" : String.Empty);
                    if (timeRemaining.Hours > 0)
                        timeString.AppendFormat("{0} hour{1} ", timeRemaining.Hours, timeRemaining.Hours > 1 ? "s" : String.Empty);
                    if (timeRemaining.Minutes > 0)
                        timeString.AppendFormat("{0} minute{1} ", timeRemaining.Minutes, timeRemaining.Minutes > 1 ? "s" : String.Empty);
                    timeString.AppendFormat("{0} second{1} remaining.", timeRemaining.Seconds, timeRemaining.Seconds > 1 ? "s" : String.Empty);

                    // Update dialog.
                    ProgressBar.Value = (int)Math.Round(value);
                    TimeRemaining = timeString.ToString();
                }
            });
        }

        /// <summary>
        /// IDispose method.
        /// </summary>
        public void Dispose()
        {
            ((IDisposable)Worker).Dispose();
        }

        #endregion

        #region Events

        /// <summary>
        /// Start the worker when the dialog has been rendered.
        /// </summary>
        private void Window_ContentRendered(object sender, EventArgs e)
        {
            StartTime = DateTime.Now;
            Worker.RunWorkerAsync();
        }

        /// <summary>
        /// Only allow close when the background work has finished. Otherwise register that a cancel has been request.
        /// </summary>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!IsDone)
                e.Cancel = true;

            if (AllowCancel)
            {
                CancelRequested = true;
                Cancel.IsEnabled = false;
            }
        }

        /// <summary>
        /// Register that a cancel event is desired.
        /// </summary>
        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            CancelRequested = true;
            Cancel.IsEnabled = false;
        }

        #endregion

    }
}
