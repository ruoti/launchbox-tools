﻿using System.Windows;
using System.Windows.Controls;

namespace LaunchBoxTools.Dialogs
{
    /// <summary>
    /// Dialog for showing text in a scrollable fashion.
    /// </summary>
    public sealed partial class ScrollableTextDialog : Window
    {

        #region Constructor

        /// <summary>
        /// Create the test window.
        /// </summary>
        private ScrollableTextDialog()
        {
            InitializeComponent();
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// Show a dialog.
        /// </summary>
        /// <param name="text">Text to show in the window.</param>
        /// <param name="title">Title of the window.</param>
        /// <param name="allowTextWrapping">Whether to allow text wrapping.</param>
        public static void Show(string text, string title = "", bool allowTextWrapping = false)
        {
            SetupWindow(text, title, allowTextWrapping).Show();
        }

        /// <summary>
        /// Show a modal dialog.
        /// </summary>
        /// <param name="text">Text to show in the window.</param>
        /// <param name="title">Title of the window.</param>
        /// <param name="allowTextWrapping">Whether to allow text wrapping.</param>
        public static void ShowDialog(string text, string title = "", bool allowTextWrapping = false)
        {
            SetupWindow(text, title, allowTextWrapping).ShowDialog();
        }

        /// <summary>
        /// Setup the window to show.
        /// </summary>
        /// <param name="text">Text to show in the window.</param>
        /// <param name="title">Title of the window.</param>
        /// <param name="allowTextWrapping">Whether to allow text wrapping.</param>
        /// <returns>The window.</returns>
        private static ScrollableTextDialog SetupWindow(string text, string title = "", bool allowTextWrapping = false)
        {
            var window = new ScrollableTextDialog() { Title = title };
            window.TextAlert.Text = text.Trim();

            if (allowTextWrapping)
            {
                window.ScrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
                window.TextAlert.TextWrapping = TextWrapping.Wrap;
            }
            else
            {
                window.ScrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
                window.TextAlert.TextWrapping = TextWrapping.NoWrap;
            }

            return window;
        }

        #endregion

        #region Events

        /// <summary>
        /// Close the window.
        /// </summary>
        private void OK_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        #endregion

    }
}
