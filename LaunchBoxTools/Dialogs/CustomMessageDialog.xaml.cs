﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Navigation;

namespace LaunchBoxTools.Dialogs
{
    /// <summary>
    /// Dialog for showing messages that are made up of a collection of <see cref="Inline" /> elements.
    /// </summary>
    public sealed partial class CustomMessageDialog : Window
    {

        #region Constructor

        /// <summary>
        /// Create the message dialog.
        /// </summary>
        private CustomMessageDialog()
        {
            InitializeComponent();
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// Show the dialog.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="text"></param>
        public static void ShowDialog(string title, IEnumerable<Inline> text)
        {
            CustomMessageDialog dialog = new CustomMessageDialog { Title = title };
            dialog.Text.Inlines.AddRange(text);
            dialog.ShowDialog();
        }

        /// <summary>
        /// Get a clickable hyperlink for the given uri.
        /// </summary>
        /// <param name="uri">Uri to get a hyper link for.</param>
        /// <param name="text">Text to display to the user instead of the uri.</param>
        /// <returns>The hyperlink object.</returns>
        public static Hyperlink GetClickableHyperlink(string uri, string text = null)
        {
            var hyperlink = new Hyperlink(new Run(text ?? uri)) { NavigateUri = new Uri(uri) };
            hyperlink.RequestNavigate += Uri_RequestNavigate;
            return hyperlink;
        }

        #endregion

        #region Events

        /// <summary>
        /// Navigate to the URI that was clicked.
        /// </summary>
        private static void Uri_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            System.Diagnostics.Process.Start(e.Uri.ToString());
        }

        /// <summary>
        /// Close the window.
        /// </summary>
        private void OK_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        #endregion

    }
}
