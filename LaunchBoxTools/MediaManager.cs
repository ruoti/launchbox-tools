﻿using LaunchBoxTools.Dialogs;
using LaunchBoxTools.NamingConventions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Xml.Linq;

using static LaunchBoxTools.LaunchBoxData;

namespace LaunchBoxTools
{
    /// <summary>
    /// Manager for all operations that act on media.
    /// </summary>
    /// <remarks>
    /// Assumes all media folders are in their default locations.
    /// </remarks>
    public sealed class MediaManager
    {

        #region Static Data

        /// <summary>
        /// Folders where media is stored in the LaunchBox directory.
        /// </summary>
        private IEnumerable<string> MediaFolders { get; } = new[] { "Images", "Manuals", "Music", "Videos" };

        /// <summary>
        /// Regex that matches media name endings (i.e., after the title).
        /// </summary>
        private static Regex MediaEndingRegex { get; } = new Regex(@"^(-\d\d)?\..+$", RegexOptions.Compiled);

        #endregion

        #region Properties

        /// <summary>
        /// Game counts, used during renaming.
        /// </summary>
        private Dictionary<Tuple<string, string>, int> GameCounts { get; }

        /// <summary>
        /// Media items that need to be renamed.
        /// </summary>
        private Dictionary<XElement, Tuple<string, string>> MediaToRename { get; } = new Dictionary<XElement, Tuple<string, string>>();

        /// <summary>
        /// New front images.
        /// </summary>
        private Dictionary<XElement, string> NewFrontImages { get; } = new Dictionary<XElement, string>();

        /// <summary>
        /// Files to rename.
        /// </summary>
        private Dictionary<string, string> FilesToRename { get; } = new Dictionary<string, string>();

        /// <summary>
        /// Games to remove.
        /// </summary>
        private HashSet<XElement> GamesToRemove { get; } = new HashSet<XElement>();

        /// <summary>
        /// Whether to replace existing files.
        /// </summary>
        private bool ReplaceExisting { get; }

        /// <summary>
        /// Whether to rename similar files during file moves.
        /// </summary>
        public bool RenameSimilar { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Create a media manager.
        /// </summary>
        /// <param name="replaceExisting">Whether to replace existing files.</param>
        public MediaManager(bool replaceExisting)
        {
            ReplaceExisting = replaceExisting;
            GameCounts = GetGameCounts();
        }

        #endregion

        #region Static Helpder Methods

        /// <summary>
        /// Get wether the game has front box art.
        /// </summary>
        /// <param name="game">Game to check.</param>
        /// <returns>Whether the game has any front box art.</returns>
        public static bool HasFrontBoxArt(XElement game)
        {
            string frontImageDirectory;
            string frontImageLocation;

            var platformName = NamingConvention.FormatFilename(GetGameValue(game, "Platform"));
            var frontImageFilename = NamingConvention.FormatFilename(GetGameValue(game, "Title"));
            
            // FrontImageTypePriorities contains the list of MediaTypes that Launchbox uses when displaying cover images. This is a prioritized list so if the first MediaType folder does not contain an image, the next available image will be used.
            string[] mediaTypes = GetLaunchBoxSetting("FrontImageTypePriorities").Split(',');

            // We need to check each possible mediaType location
            foreach (string mediaType in mediaTypes)
            {
                // Image locations can be overridden in Launchbox so we need to get the proper location for the given mediaType
                frontImageLocation = GetPlatformFolder(platformName, mediaType);
                
                frontImageDirectory = Path.Combine(LaunchBoxDirectory, frontImageLocation);

                // As soon as we find an image that Launchbox will use as a front image we will return true
                if ((Directory.Exists(frontImageDirectory)) && (Directory.EnumerateFiles(frontImageDirectory, frontImageFilename + "*.*", SearchOption.TopDirectoryOnly).Any()))
                {
                    return true;
                }
            }

            return false;
        }

        #endregion

        #region Title Rename Methods

        /// <summary>
        /// Enqueue a rename operation.
        /// </summary>
        /// <param name="game">Game that is being renamed.</param>
        /// <param name="originalTitle">Original title of the game.</param>
        /// <param name="newTitle">New title of the game.</param>
        public void EnqueueTitleRename(XElement game, string originalTitle, string newTitle)
        {
            if (originalTitle.Equals(newTitle, StringComparison.InvariantCultureIgnoreCase))
                return;
            MediaToRename[game] = Tuple.Create(originalTitle, newTitle);
        }

        /// <summary>
        /// Process the title renames.
        /// </summary>
        /// <param name="isTest">Whether this operation is just a test.</param>
        /// <returns>If isTest is true, returns a string describing the results, otherwise returns null.</returns>
        public string ProcessTitleRenames(bool isTest = false)
        {
            StringBuilder testResults = isTest ? new StringBuilder() : null;

            // Move files based on title changes.
            if (MediaToRename.Count > 0)
            {
                if (isTest)
                    testResults.Append("Media moved because of title change.\n\n");

                ProgressDialog.ShowDialog(dialog =>
                {
                    double totalIterations = MediaToRename.Count, currentIteration = 0;

                    foreach (var pair in MediaToRename)
                    {
                        // Report on the current prgoress.
                        currentIteration++;
                        dialog.ReportProgress((currentIteration / totalIterations) * 100, subText: string.Format("Game {0} of {1}.", currentIteration, totalIterations));

                        // Get the values for the move operation.
                        var game = pair.Key;
                        var originalTitle = pair.Value.Item1;
                        var newTitle = pair.Value.Item2;
                        var platform = GetGameValue(game, "Platform");

                        var originalTitleFilename = NamingConvention.FormatFilename(originalTitle);
                        var newTitleFilename = NamingConvention.FormatFilename(newTitle);
                        var platformFileName = NamingConvention.FormatFilename(platform);

                        // Move the file if it is the only item with this title, platform combination, otherwise copy the file.
                        var gameCountKey = Tuple.Create(platform, originalTitle);
                        var remainingGames = GameCounts[gameCountKey] - 1;
                        GameCounts[gameCountKey] = remainingGames;
                        var move = (remainingGames == 0);

                        // Add line in the report for this game.
                        if (isTest)
                            testResults.AppendFormat("Game {0}: {1} -> {2}\n", GetGameValue(game, "ApplicationPath"), originalTitle, newTitle);

                        // Iterate over the media directories.
                        foreach (var folder in MediaFolders.Select(f => Path.Combine(LaunchBoxDirectory, f, platformFileName)))
                        {
                            if (!Directory.Exists(folder))
                                continue;

                            // Find existing media, and move to the new destination.
                            foreach (var file in Directory.EnumerateFiles(folder, originalTitleFilename + "*", SearchOption.AllDirectories))
                            {
                                try
                                {
                                    var fileEnding = Path.GetFileName(file).Substring(originalTitleFilename.Length);
                                    if (MediaEndingRegex.IsMatch(fileEnding))
                                    {
                                        var newFile = Path.Combine(Path.GetDirectoryName(file), newTitleFilename + fileEnding);
                                        if (ReplaceExisting || !File.Exists(newFile))
                                        {
                                            if (isTest)
                                                testResults.AppendFormat("{0} {1} -> {2}\n", move ? "Move" : "Copy", file, newFile);
                                            else
                                            {
                                                if (move)
                                                {
                                                    if (File.Exists(newFile))
                                                        File.Delete(newFile);
                                                    File.Move(file, newFile);
                                                }
                                                else
                                                    File.Copy(file, newFile, true);
                                            }
                                        }
                                    }
                                }
                                catch (Exception e)
                                {
                                    MessageBox.Show(string.Format("{0}\n\n{1}", "Error moving media.", e.ToString()), "File Move Error", MessageBoxButton.OK, MessageBoxImage.Error);
                                }
                            }
                        }

                        if (isTest)
                            testResults.Append("\n");
                    }
                }, "Updating Media", "Updating media for changed titles...", allowCancel: false);
                MediaToRename.Clear();
            }

            return isTest ? testResults.ToString() : null;
        }

        #endregion

        #region New Front Image Methods

        /// <summary>
        /// Enqueue a copy operation.
        /// </summary>
        /// <param name="game">Game with the new front image.</param>
        /// <param name="frontImageFile">New front image file.</param>
        public void EnqueueNewFrontImage(XElement game, string frontImageFile)
        {
            if (!File.Exists(frontImageFile))
                return;
            NewFrontImages[game] = frontImageFile;
        }

        /// <summary>
        /// Process the new front image file.
        /// </summary>
        /// <param name="isTest">Whether this operation is just a test.</param>
        /// <returns>If isTest is true, returns a string describing the results, otherwise returns null.</returns>
        public string ProcessNewFrontImages(bool isTest = false)
        {
            StringBuilder testResults = isTest ? new StringBuilder() : null;

            // Copy the enqueued files.
            if (NewFrontImages.Count > 0)
            {
                if (isTest)
                    testResults.Append("New front images.\n\n");

                ProgressDialog.ShowDialog(dialog =>
                {
                    double totalIterations = NewFrontImages.Count, currentIteration = 0;

                    foreach (var pair in NewFrontImages)
                    {
                        // Report on the current prgoress.
                        currentIteration++;
                        dialog.ReportProgress((currentIteration / totalIterations) * 100, subText: string.Format("Game {0} of {1}.", currentIteration, totalIterations));

                        // Get values from the pair.
                        var game = pair.Key;
                        var imageFile = pair.Value;
                        var frontImageDirectory = Path.Combine(LaunchBoxDirectory, "Images", NamingConvention.FormatFilename(GetGameValue(game, "Platform")), "Front");
                        var frontImageFilename = NamingConvention.FormatFilename(GetGameValue(game, "Title"));
                        var newImageFile = Path.Combine(frontImageDirectory, Path.GetFileName(frontImageFilename) + Path.GetExtension(imageFile));

                        // Check that the operation is still valid.
                        if (!File.Exists(imageFile))
                            continue;

                        // Remove existing file if necessary.
                        var existingFrontImageFile = Directory.EnumerateFiles(frontImageDirectory, frontImageFilename + ".*", SearchOption.TopDirectoryOnly)
                                                        .SingleOrDefault();
                        if (existingFrontImageFile != null)
                        {
                            if (!ReplaceExisting)
                                continue;

                            if (isTest)
                                testResults.AppendFormat("Deleting {0}\n", existingFrontImageFile);
                            else
                                File.Delete(existingFrontImageFile);
                        }

                        // Copy the file.
                        if (isTest)
                            testResults.AppendFormat("Copying {0} -> {1}\n", imageFile, newImageFile);
                        else
                            File.Copy(imageFile, newImageFile);
                    }
                }, "Updating Media", "Copying files...", allowCancel: false);
                NewFrontImages.Clear();
            }

            return isTest ? testResults.ToString() : null;
        }

        #endregion

        #region Move File Methods

        /// <summary>
        /// Enqueue a rename operation.
        /// </summary>
        /// <param name="originalFile">Original absolute file path.</param>
        /// <param name="newFile">New absolute file path.</param>
        public void EnqueueFileRename(string originalFile, string newFile)
        {
            if (!File.Exists(originalFile))
                return;

            if (RenameSimilar)
            {
                var originalDirectory = Path.GetDirectoryName(originalFile);
                var originalFilename = Path.GetFileNameWithoutExtension(originalFile);
                var newDirectory = Path.GetDirectoryName(newFile);
                var newFilename = Path.GetFileNameWithoutExtension(newFile);

                foreach (var similarFile in Directory.EnumerateFiles(originalDirectory, originalFilename + ".*", SearchOption.TopDirectoryOnly))
                {
                    var newSimilarFile = Path.Combine(newDirectory, newFilename + Path.GetExtension(similarFile));
                    if (!similarFile.Equals(newSimilarFile, StringComparison.InvariantCultureIgnoreCase))
                        FilesToRename[similarFile] = newSimilarFile;
                }
            }
            else
            {
                FilesToRename[originalFile] = newFile;

            }

        }

        /// <summary>
        /// Process the file renames.
        /// </summary>
        /// <param name="isTest">Whether this operation is just a test.</param>
        /// <returns>Returns information on what occurred during the operation. Null indicates there were no conflicts and the files were moved.
        /// If isTest is true, the return also includes information about what occurred.</returns>
        public string ProcessFileRenames(bool isTest = false)
        {
            var testResults = isTest ? new StringBuilder() : null;

            // Copy the enqueued files.
            if (FilesToRename.Count > 0)
            {
                // Check for conflicts.
                var conflicts = GetFileRenameConflicts();
                if (!string.IsNullOrEmpty(conflicts))
                    return "Conflicts:\n" + conflicts.ToString();

                // Move files; we can do trivially because we know there are no conflicts.
                if (isTest)
                    testResults.Append("Moving files.\n\n");

                ProgressDialog.ShowDialog(dialog =>
                {
                    double totalIterations = FilesToRename.Count, currentIteration = 0;

                    foreach (var pair in FilesToRename)
                    {
                        // Report on the current prgoress.
                        currentIteration++;
                        dialog.ReportProgress((currentIteration / totalIterations) * 100, subText: string.Format("File {0} of {1}.", currentIteration, totalIterations));

                        // Get values from the pair.
                        var originalFile = pair.Key;
                        var originalDirectory = Path.GetDirectoryName(originalFile);
                        var newFile = pair.Value;
                        var newDirectory = Path.GetDirectoryName(newFile);

                        // Make sure the destination directory exists.
                        if (!Directory.Exists(newDirectory))
                        {
                            if (isTest)
                                testResults.AppendFormat("Creating directory {0}\n", newDirectory);
                            else
                                Directory.CreateDirectory(newDirectory);
                        }

                        // Move the file.
                        if (isTest)
                            testResults.AppendFormat("Moving {0} -> {1}\n", originalFile, newFile);
                        else
                            File.Move(originalFile, newFile);

                        // Clean up empty directories.
                        if (!Directory.EnumerateFiles(originalDirectory, "*", SearchOption.AllDirectories).Any())
                        {
                            if (isTest)
                                testResults.AppendFormat("Deleting directory {0}\n", originalDirectory);
                            else
                                Directory.Delete(originalDirectory);
                        }
                    }
                }, "Updating Media", "Moving files...", allowCancel: false);
                NewFrontImages.Clear();
            }

            return isTest ? testResults.ToString() : null;
        }

        /// <summary>
        /// Get conflicts for file renaming.
        /// </summary>
        /// <returns>String containing a description of the conflicts.</returns>
        private string GetFileRenameConflicts()
        {
            // Go through all the queued updates and check for conflicts.
            var reverseDictionary = new Dictionary<string, string>();
            var conflicts = new StringBuilder();

            foreach (var pair in FilesToRename)
            {
                var originalFile = pair.Key;
                var newFile = pair.Value;

                if (File.Exists(newFile))
                    conflicts.AppendFormat("Destination file already exists: {0} -> {1}\n", originalFile, newFile);
                if (reverseDictionary.ContainsKey(newFile))
                    conflicts.AppendFormat("File rename conflicts: {0}, {1} -> {2}\n", originalFile, reverseDictionary[newFile], newFile);
                reverseDictionary[newFile] = originalFile;
            }

            return conflicts.ToString();
        }

        #endregion

        #region Rename Platform Methods

        /// <summary>
        /// Move the platform media files.
        /// </summary>
        /// <param name="newPlatformName">The new platform name.</param>
        /// <param name="isTest">Whether this operation is just a test.</param>
        /// <returns>If isTest is true, the return includes information about what occurred.</returns>
        public string MovePlatformFiles(string newPlatformName, bool isTest = false)
        {
            var testResults = isTest ? new StringBuilder() : null;

            // Copy the enqueued files.
            if (SelectedPlatforms.Any())
            {
                // Move files; we can do trivially because we know there are no conflicts.
                if (isTest)
                    testResults.Append("Moving files.\n\n");

                ProgressDialog.ShowDialog(dialog =>
                {
                    double totalIterations = SelectedPlatforms.Count(), currentIteration = 0;

                    foreach (var platform in SelectedPlatforms)
                    {
                        // Report on the current prgoress.
                        currentIteration++;
                        dialog.ReportProgress((currentIteration / totalIterations) * 100, subText: string.Format("Platform {0} of {1}.", currentIteration, totalIterations));

                        var originalPlatformFilename = NamingConvention.FormatFilename(platform);
                        var newPlatformFilename = NamingConvention.FormatFilename(newPlatformName);
                        if (string.Equals(originalPlatformFilename, newPlatformFilename))
                            continue;


                        foreach (var folder in MediaFolders.Union(MediaFolders.Select(f => Path.Combine(f, "Platforms"))).Select(f => Path.Combine(LaunchBoxDirectory, f, originalPlatformFilename)))
                        {
                            if (!Directory.Exists(folder))
                                continue;

                            // Find existing media, and move to the new destination.
                            var newPlatformDirectory = Path.Combine(Path.GetDirectoryName(folder), newPlatformFilename);
                            foreach (var file in Directory.EnumerateFiles(folder, "*", SearchOption.AllDirectories))
                            {
                                var newDirectory = Path.Combine(newPlatformDirectory, Path.GetDirectoryName(file).Substring(folder.Length + 1));
                                if (!Directory.Exists(newDirectory))
                                {
                                    if (isTest)
                                        testResults.AppendFormat("Create directory {0}\n", newDirectory);
                                    else
                                        Directory.CreateDirectory(newDirectory);
                                }

                                var newFile = Path.Combine(newDirectory, Path.GetFileName(file));
                                if (ReplaceExisting || !File.Exists(newFile))
                                {
                                    if (isTest)
                                        testResults.AppendFormat("Move {0} -> {1}\n", file, newFile);
                                    else
                                        File.Move(file, newFile);
                                }
                            }

                            // Delete the old directory if it is empty.
                            if (!Directory.EnumerateFiles(folder, "*", SearchOption.AllDirectories).Any())
                            {
                                if (isTest)
                                    testResults.AppendFormat("Delete directory {0}\n", folder);
                                else
                                    Directory.Delete(folder, true);
                            }
                        }
                    }
                }, "Updating Media", "Moving files...", allowCancel: false);
                NewFrontImages.Clear();
            }

            return isTest ? testResults.ToString() : null;
        }

        #endregion

        #region Remove Games Method

        /// <summary>
        /// Enqueue a remove operation.
        /// </summary>
        /// <param name="game">Game that is being removed.</param>
        public void EnqueueGameRemoval(XElement game)
        {
            GamesToRemove.Add(game);
        }

        /// <summary>
        /// Process the title renames.
        /// </summary>
        /// <param name="isTest">Whether this operation is just a test.</param>
        /// <returns>If isTest is true, returns a string describing the results, otherwise returns null.</returns>
        public string ProcessGameRemovals(bool isTest = false)
        {
            StringBuilder testResults = isTest ? new StringBuilder() : null;

            // Move files based on title changes.
            if (MediaToRename.Count > 0)
            {
                if (isTest)
                    testResults.Append("Media deleted.\n\n");

                ProgressDialog.ShowDialog(dialog =>
                {
                    double totalIterations = MediaToRename.Count, currentIteration = 0;

                    foreach (var game in GamesToRemove)
                    {
                        // Report on the current prgoress.
                        currentIteration++;
                        dialog.ReportProgress((currentIteration / totalIterations) * 100, subText: string.Format("Game {0} of {1}.", currentIteration, totalIterations));

                        // Get the values for the move operation.
                        var title = GetGameValue(game, "Title");
                        var platform = GetGameValue(game, "Platform");

                        var titleFilename = NamingConvention.FormatFilename(title);
                        var platformFileName = NamingConvention.FormatFilename(platform);

                        // Move the file if it is the only item with this title, platform combination, otherwise copy the file.
                        var gameCountKey = Tuple.Create(platform, title);
                        var remainingGames = GameCounts[gameCountKey] - 1;
                        GameCounts[gameCountKey] = remainingGames;

                        // Only delete if there are no games that relly on this media.
                        if (remainingGames != 0)
                            continue;

                        // Add line in the report for this game.
                        if (isTest)
                            testResults.AppendFormat("Game {0}\n", GetGameValue(game, "ApplicationPath"));

                        // Iterate over the media directories.
                        foreach (var folder in MediaFolders.Select(f => Path.Combine(LaunchBoxDirectory, f, platformFileName)))
                        {
                            if (!Directory.Exists(folder))
                                continue;

                            // Find existing media, and move to the new destination.
                            foreach (var file in Directory.EnumerateFiles(folder, titleFilename + "*", SearchOption.AllDirectories).ToList())
                            {
                                try
                                {
                                    var fileEnding = Path.GetFileName(file).Substring(titleFilename.Length);
                                    if (MediaEndingRegex.IsMatch(fileEnding))
                                    {
                                        if (isTest)
                                            testResults.AppendFormat("Deleting file {0}.\n", file);
                                        else
                                            File.Delete(file);
                                    }
                                }
                                catch (Exception e)
                                {
                                    MessageBox.Show(string.Format("{0}\n\n{1}", "Error moving media.", e.ToString()), "File Move Error", MessageBoxButton.OK, MessageBoxImage.Error);
                                }
                            }

                            if (!Directory.EnumerateFiles(folder, "*", SearchOption.AllDirectories).Any())
                            {
                                if (isTest)
                                    testResults.AppendFormat("Deleting folder {0}.\n", folder);
                                else
                                    Directory.Delete(folder);
                            }
                        }

                        if (isTest)
                            testResults.Append("\n");
                    }
                }, "Updating Media", "Updating media for changed titles...", allowCancel: false);
                MediaToRename.Clear();
            }

            return isTest ? testResults.ToString() : null;
        }

        #endregion

    }
}
