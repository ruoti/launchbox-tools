﻿namespace LaunchBoxTools.Converters
{
    /// <summary>
    /// Converts an object to a bool based on whether its string representation is null or empty.
    /// The bool is then changed into a value using <see cref="BoolToValueConverter{T}"/>.
    /// </summary>
    public abstract class IsNullOrWhitespaceToValueConverter<T>: BoolToValueConverter<T>
    {

        /// <summary>
        /// Convert the value to a string and checks if it is null or white space.
        /// </summary>
        /// <param name="value">Value to convert.</param>
        /// <returns>True if the value is only white space.</returns>
        protected override bool Convert(object value)
        {
            if (null == value)
                return false;
            return string.IsNullOrWhiteSpace(value.ToString());
        }

    }
}
