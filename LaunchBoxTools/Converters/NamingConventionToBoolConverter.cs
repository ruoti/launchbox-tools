﻿using LaunchBoxTools.NamingConventions;
using System;
using System.Globalization;
using System.Windows.Data;

namespace LaunchBoxTools.Converters
{

    /// <summary>
    /// Converts a <see cref="NamingConvention"/> into a bool indicating whether it is a custom convention.
    /// </summary>
    [ValueConversion(typeof(NamingConvention), typeof(bool))]
    public sealed class NamingConventionToBoolConverter: IValueConverter
    {

        #region Constructor

        /// <summary>
        /// Create the converter.
        /// </summary>
        public NamingConventionToBoolConverter() { }

        #endregion

        #region Methods

        /// <summary>
        /// Converts a boolean to a value.
        /// </summary>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType != typeof(bool))
                throw new InvalidOperationException("The target must be a bool.");
            return null == value;
        }

        /// <summary>
        /// Convert back from the value to a boolean, where false is a catch all.
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        #endregion

    }

}
