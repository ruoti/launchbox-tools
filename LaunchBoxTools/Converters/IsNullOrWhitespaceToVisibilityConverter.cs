﻿using System.Windows;
using System.Windows.Data;

namespace LaunchBoxTools.Converters
{
    /// <summary>
    /// Converts an object to a bool based on whether its string representation is null or empty.
    /// The bool is then changed into a <see cref="Visibility"/> using <see cref="BoolToValueConverter{T}"/>.
    /// </summary>
    [ValueConversion(typeof(object), typeof(Visibility))]
    public sealed class IsNullOrWhitespaceToVisibilityConverter: IsNullOrWhitespaceToValueConverter<Visibility>
    {

        /// <summary>
        /// Create the converter.
        /// </summary>
        public IsNullOrWhitespaceToVisibilityConverter()
        {
            TrueValue = Visibility.Collapsed;
            FalseValue = Visibility.Visible;
        }

    }
}
