﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace LaunchBoxTools.Converters
{

    /// <summary>
    /// Base class for an <see cref="IValueConverter"/> which takes a bool and changes it to some value.
    /// </summary>
    public abstract class BoolToValueConverter<T>: MarkupExtension, IValueConverter
    {

        #region Properties

        /// <summary>
        /// Value to convert to when input is true.
        /// </summary>
        public T TrueValue { get; set; }

        /// <summary>
        /// Value to convert to when input is false.
        /// </summary>
        public T FalseValue { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Convert the value to a bool.
        /// </summary>
        /// <param name="value">Value to convert.</param>
        /// <returns>Bool based on the value.</returns>
        protected virtual bool Convert(object value)
        {
            return System.Convert.ToBoolean(value);
        }

        /// <summary>
        /// Converts a boolean to a value.
        /// </summary>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType != typeof(T))
                throw new InvalidOperationException("The target must be the correct value type.");
            return Convert(value) ? TrueValue : FalseValue;
        }

        /// <summary>
        /// Convert back from the value to a boolean, where false is a catch all.
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value.Equals(TrueValue);
        }

        /// <summary>
        /// Returns the object which will have properties set.
        /// </summary>
        /// <param name="serviceProvider">Ignored</param>
        /// <returns>This.</returns>
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

        #endregion

    }

}
