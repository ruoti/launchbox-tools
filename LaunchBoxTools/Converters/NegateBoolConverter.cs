﻿using System.Windows.Data;

namespace LaunchBoxTools.Converters
{
    /// <summary>
    /// Converter that negates the passed in value.
    /// </summary>
    [ValueConversion(typeof(bool), typeof(bool))]
    public sealed class NegateBoolConverter : BoolToValueConverter<bool>
    {

        /// <summary>
        /// Create the converter.
        /// </summary>
        public NegateBoolConverter()
        {
            TrueValue = false;
            FalseValue = true;
        }

    }
}
