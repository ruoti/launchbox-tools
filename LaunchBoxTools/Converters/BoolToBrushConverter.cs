﻿using System.Windows.Data;
using System.Windows.Media;

namespace LaunchBoxTools.Converters
{
    /// <summary>
    /// Converts a bool into a <see cref="Brush"/>.
    /// </summary>
    [ValueConversion(typeof(bool), typeof(Brush))]
    public sealed class BoolToBrushConverter : BoolToValueConverter<Brush>
    {

        /// <summary>
        /// Create the converter.
        /// </summary>
        public BoolToBrushConverter() { }

    }
}
