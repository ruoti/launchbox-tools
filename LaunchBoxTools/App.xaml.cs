﻿using System;
using System.Linq;
using System.Reflection;
using System.Windows;

namespace LaunchBoxTools
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    partial class App : Application
    {
        /// <summary>
        /// Startup code.
        /// </summary>
        public App()
        {
            // Allow the .exe to find embedded .dll files.
            AppDomain.CurrentDomain.AssemblyResolve += (sender, args) =>
            {
                // Check to see that we have this dll before trying to load it using reflection.
                string resourceName = "LaunchBoxTools.dll." + new AssemblyName(args.Name).Name + ".dll";
                if (!Assembly.GetExecutingAssembly().GetManifestResourceNames().Contains(resourceName))
                    return null;

                using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName))
                {
                    Byte[] assemblyData = new Byte[stream.Length];
                    stream.Read(assemblyData, 0, assemblyData.Length);
                    return Assembly.Load(assemblyData);
                }
            };
        }

    }
}
