﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace LaunchBoxTools.NamingConventions
{
    /// <summary>
    /// Class for handling the No-Intro (2015-03-24) naming convention.
    /// </summary>
    public sealed class NoIntro_2007_10_30 : NamingConvention
    {

        #region Static Data

        /// <summary>
        /// Static instance of the No-Intro naming convention.
        /// </summary>
        public static NoIntro_2007_10_30 Instance { get; } = new NoIntro_2007_10_30();

        /// <summary>
        /// Name of this naming convention.
        /// </summary>
        public static string Name { get; } = "No-Intro (2007-10-30)";

        /// <summary>
        /// List of languages recognized by the NoIntro convention.
        /// </summary>
        private static IEnumerable<string> Languages { get; } = new[] { "En", "Ja", "Fr", "De", "Es", "It", "Nl", "Pt", "Sv", "No", "Da", "Fi", "Zh", "Ko", "Pl" };

        /// <summary>
        /// Regex to match languages.
        /// </summary>
        private static Regex LanguagesRegex { get; } = new Regex("^(" + string.Join("|", Languages) + ")(,(" + string.Join("|", Languages) + "))*$", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        /// <summary>
        /// Regex to match versions.
        /// </summary>
        private static Regex VersionRegex { get; } = new Regex(@"^( v[^)]+ | Rev\s[^)]+ | (Proto|Alpha|Beta|Sample)\s*[^)]* )$", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);

        /// <summary>
        /// Regex to match versions.
        /// </summary>
        private static Regex Version1Regex { get; } = new Regex(@"\s\(v1\.0+\)", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        /// <summary>
        /// Regex to match disc numbers.
        /// </summary>
        private static Regex DiscRegex { get; } = new Regex(@"^Disc\s\d+$", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        /// <summary>
        /// Regex to match disc numbers.
        /// </summary>
        private static Regex DiskRegex { get; } = new Regex(@"^Disk\s\d+$", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        /// <summary>
        /// Regex to match unlicensed field.
        /// </summary>
        private static Regex LicenseRegex { get; } = new Regex(@"^Unl$", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        /// <summary>
        /// Regex to match broken field.
        /// </summary>
        private static Regex StatusRegex { get; } = new Regex(@"^b$", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a No-Intro 2007-10-30 naming convention.
        /// </summary>
        private NoIntro_2007_10_30() : base("/Title|?/ (/Region|?/)/Language| (?)|o//Version| (?)|o//Disc| (?)|o//Disk| (?)|o//Miscellaneous| (?)|r//License| (?)|o//DumpStatus| [?]|o/") { }

        #endregion

        #region Methods

        /// <summary>
        /// Parse the attributes from the filename.
        /// </summary>
        /// <param name="filename">Filename to parse.</param>
        /// <returns>Fields extracted from the filename.</returns>
        public override IDictionary<string, string> Parse(string filename)
        {
            var fields = new Dictionary<string, string>();
            var miscellaneous = new List<string>();

            // Get the title. Handle No-Intro's title article style.
            var title = RemoveExtraInfoTags(filename).Replace(" -", ":");
            if (!string.IsNullOrEmpty(title))
            {
                fields["SortTitle"] = MoveArticleToBack(title);
                fields["Title"] = title = MoveArticleToFront(title);
            }

            // Pull out the extra fields and parse them.
            bool regionMatched = false;
            foreach (Match match in ExtraInfoRegex.Matches(filename))
            {
                var matchValue = match.Value.Substring(1, match.Value.Length - 2).Trim();
                if (string.IsNullOrEmpty(matchValue))
                    continue;

                if (!regionMatched)
                {
                    if (match.Value.StartsWith("["))
                        continue;
                    fields["Region"] = matchValue;
                    regionMatched = true;
                }
                else
                {
                    if (LanguagesRegex.IsMatch(matchValue))
                        fields["Language"] = matchValue;
                    else if (VersionRegex.IsMatch(matchValue))
                        fields["Version"] = matchValue;
                    else if (DiscRegex.IsMatch(matchValue))
                        fields["Disc"] = matchValue;
                    else if (DiskRegex.IsMatch(matchValue))
                        fields["Disk"] = matchValue;
                    else if (LicenseRegex.IsMatch(matchValue))
                        fields["License"] = matchValue;
                    else if (StatusRegex.IsMatch(matchValue))
                        fields["DumpStatus"] = matchValue;
                    else
                        miscellaneous.Add(matchValue);
                }
            }

            // Include any miscellaneous fields
            if (miscellaneous.Count > 0)
                fields["Miscellaneous"] = String.Join(";", miscellaneous);

            return fields;
        }

        /// <summary>
        /// Format a string from the game's metadata.
        /// </summary>
        /// <param name="game">Game to get meta data for.</param>
        /// <returns>Formatted string.</returns>
        public override string Format(XElement game)
        {
            var title = base.Format(game);

            // Remove (v1.0) from the title.
            title = Version1Regex.Replace(title, string.Empty);

            // Move the article to the 
            return MoveArticleToBack(title);
        }

        #endregion

    }
}
