﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace LaunchBoxTools.NamingConventions
{
    /// <summary>
    /// Class for handling the Tosec (2015-03-24) naming convention.
    /// </summary>
    public sealed class Tosec_2015_03_24: NamingConvention
    {

        #region Static Data

        /// <summary>
        /// Name of this naming convention.
        /// </summary>
        public static string Name { get; } = "Tosec (2015-03-24)";

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a No-Intro 2007-10-30 naming convention.
        /// </summary>
        public Tosec_2015_03_24() : base("/Title|?/ (/Region|?/)/Languages| (?)|o//Version| (?)|o//DiscNumber| (?)|o//DiskNumber| (?)|o//Miscellaneous| (?)|r//License| (?)|o//DumpStatus| [?]|o/") { }

        #endregion

        #region Methods

        /// <summary>
        /// Parse the attributes from the filename.
        /// </summary>
        /// <param name="filename">Filename to parse.</param>
        /// <returns>Fields extracted from the filename.</returns>
        public override IDictionary<string, string> Parse(string filename)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Format a string from the game's metadata.
        /// </summary>
        /// <param name="game">Game to get meta data for.</param>
        /// <returns>Formatted string.</returns>
        public override string Format(XElement game)
        {
            throw new NotSupportedException();
        }

        #endregion

    }
}
