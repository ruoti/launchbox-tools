﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace LaunchBoxTools.NamingConventions
{
    /// <summary>
    /// Naming convention base class.
    /// </summary>
    public class NamingConvention
    {

        #region Static Data

        /// <summary>
        /// The list of 
        /// </summary>
        public static IEnumerable<KeyValuePair<string, NamingConvention>> NamingConventions { get; } = new[] {
            new KeyValuePair<string, NamingConvention>(NoIntro_2007_10_30.Name, NoIntro_2007_10_30.Instance),
            //new KeyValuePair(Tosec_2015_03_24.Name, new Tosec_2015_03_24.Instance),
            new KeyValuePair<string, NamingConvention>("Custom", null)
        };

        /// <summary>
        /// Regular expression for matching fields in filename format strings.
        /// </summary>
        protected static Regex FieldRegex { get; } = new Regex(@"\/ (?<field>\w+) \| (?<replacement>[^|/]+) (\| (?<options>(o|optional|r|repeated)) )? \/", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);

        /// <summary>
        /// Regex to match entries we want to remove from the title.
        /// </summary>
        protected static Regex ExtraInfoRegex { get; } = new Regex(@"\([^)]*\)|\[[^\]]*\]|\{[^}]*\}", RegexOptions.Compiled);

        /// <summary>
        /// Characters that start extra info sections in a title or filename.
        /// </summary>
        protected static char[] ExtraInfoStartingChars { get; } = new char[] { '[', '(', '{' };

        /// <summary>
        /// Regex to remove invalid characters from filenames.
        /// </summary>
        protected static Regex InvalidCharacterRegex { get; } = new Regex(@"[" + string.Join(string.Empty, Path.GetInvalidPathChars().Union(Path.GetInvalidFileNameChars()).Distinct().Select(c => c.ToString())) + "']", RegexOptions.Compiled);

        /// <summary>
        /// List of commong articles used in modifying the title.
        /// </summary>
        protected static IEnumerable<string> TitleArticles { get; } = new[] { "A", "An", "The", "Der", "Den", "Dem", "Des", "Das", "Die", "Le", "La", "L'", "Les", "Un", "Une" };

        /// <summary>
        /// Regex for detecting titles at the end of the string.
        /// </summary>
        protected static Regex TitleArticleBackRegex { get; } = new Regex(@",\s?(?<article>(" + string.Join("|", TitleArticles) + "))$", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        /// <summary>
        /// Regex for detecting titles at the end of the string.
        /// </summary>
        protected static Regex TitleArticleFrontRegex { get; } = new Regex(@"^(?<article>(" + string.Join("|", TitleArticles) + @"))\s", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        #endregion

        #region Static Methods

        /// <summary>
        /// Move the article in a title to the front of the title.
        /// </summary>
        /// <param name="title">Title to modify.</param>
        /// <param name="seperator">Seperator to use, null if the whole title should be used.</param>
        /// <returns>Title with the article at the front.</returns>
        public static string MoveArticleToFront(string title, string seperator = ":")
        {
            // Store extra information to be added back to the title in the end.
            var titleToChange = title;
            var startOfExtra = title.IndexOfAny(ExtraInfoStartingChars);
            if (startOfExtra != -1)
                titleToChange = title.Substring(0, startOfExtra).Trim();

            var parts = seperator == null ? new string[] { titleToChange } : titleToChange.Split(new string[] { seperator }, StringSplitOptions.None);
            var match = TitleArticleBackRegex.Match(parts[0]);
            if (match.Success)
                parts[0] = match.Groups["article"].Value + " " + parts[0].Substring(0, parts[0].Length - match.Value.Length);
            return string.Join(seperator, parts) + title.Substring(titleToChange.Length);
        }

        /// <summary>
        /// Move the article in a title to the back of the title.
        /// </summary>
        /// <param name="title">Title to modify.</param>
        /// <param name="seperator">Seperator to use, null if the whole title should be used.</param>
        /// <returns>Title with the article at the back.</returns>
        public static string MoveArticleToBack(string title, string seperator = ":")
        {
            // Store extra information to be added back to the title in the end.
            var titleToChange = title;
            var startOfExtra = title.IndexOfAny(ExtraInfoStartingChars);
            if (startOfExtra != -1)
                titleToChange = title.Substring(0, startOfExtra).Trim();

            var parts = seperator == null ? new string[] { titleToChange } : titleToChange.Split(new[] { seperator }, StringSplitOptions.None);
            var match = TitleArticleFrontRegex.Match(parts[0]);
            if (match.Success)
                parts[0] = parts[0].Substring(match.Length) + ", " + match.Groups["article"].Value;
            return string.Join(seperator, parts) + title.Substring(titleToChange.Length);
        }

        /// <summary>
        /// Replaces invalid filename characters.
        /// </summary>
        /// <param name="filename">Name to prepare.</param>
        /// <param name="invalidCharacterReplacement">Replacement string for characters not allowed in filenames.</param>
        /// <param name="colonReplacement">Replacement value for the colon, which is commonly found in games.</param>
        /// <returns>Filename ready for use in a filesystem.</returns>
        public static string FormatFilename(string filename, string invalidCharacterReplacement = "_", string colonReplacement = null)
        {
            if (colonReplacement != null)
                filename = filename.Replace(":", colonReplacement);
            filename = InvalidCharacterRegex.Replace(filename, invalidCharacterReplacement ?? string.Empty);
            return filename.Trim();
        }

        /// <summary>
        /// Replaces extra info tags (i.e., (), [], {}).
        /// </summary>
        /// <param name="name">Name to prepare.</param>
        /// <returns>Name without extra info tags.</returns>
        public static string RemoveExtraInfoTags(string name)
        {
            return ExtraInfoRegex.Replace(name, string.Empty).Trim();
        }

        #endregion

        #region Properties

        protected string FormatString { get; }

        #endregion

        #region Constructor

        /// <summary>
        /// Create a naming convention with the given format string.
        /// </summary>
        /// <param name="formatString">Format string to use with ToFilename.</param>
        protected NamingConvention(string formatString)
        {
            FormatString = formatString;
        }

        /// <summary>
        /// Create a custom naming convention. Does not support ParseFilename.
        /// </summary>
        /// <param name="formatString">Format string to use with ToFilename.</param>
        /// <returns>Custom naming convention.</returns>
        public static NamingConvention GetCustomNamingConvention(string formatString)
        {
            return new NamingConvention(formatString);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Parse the attributes from the filename.
        /// </summary>
        /// <param name="filename">Filename to parse.</param>
        /// <returns>Fields extracted from the filename.</returns>
        public virtual IDictionary<string, string> Parse(string filename)
        {
            var values = new Dictionary<string, string>();
            var title = RemoveExtraInfoTags(filename).Replace(" -", ":");

            if (!string.IsNullOrEmpty(title))
            {
                values["Title"] = MoveArticleToFront(title);
                values["SortTitle"] = MoveArticleToBack(title);
            }

            return values;
        }

        /// <summary>
        /// Format a string from the game's metadata.
        /// </summary>
        /// <param name="game">Game to get meta data for.</param>
        /// <returns>Formatted string.</returns>
        public virtual string Format(XElement game)
        {
            var name = FormatString;

            foreach (Match match in FieldRegex.Matches(name))
            {
                var value = LaunchBoxData.GetGameValue(game, match.Groups["field"].Value);
                var replacement = match.Groups["replacement"].Value;
                var options = match.Groups["options"].Value;
                var replacementString = string.Empty;

                // Skip if optional or repeated and there is no values, then don't include it.
                if (string.IsNullOrEmpty(value) && !string.IsNullOrEmpty(options))
                    replacementString = string.Empty;
                // Make single replacement
                else if (!options.Equals("repeated", StringComparison.InvariantCultureIgnoreCase) && !options.Equals("r", StringComparison.InvariantCultureIgnoreCase))
                    replacementString = replacement.Replace("?", value);
                // Make multiple replacements.
                else
                    replacementString = String.Join(string.Empty, value.Split(';').Select(subValue => replacement.Replace("?", subValue)));

                name = name.Replace(match.Value, replacementString);
            }

            return name.Trim();
        }

        #endregion

    }
}
