﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace LaunchBoxTools.NamingConventions
{
    /// <summary>
    /// A control for selecting which naming convention ot use.
    /// </summary>
    public sealed partial class SelectorControl : UserControl
    {

        #region Dependency Properties

        /// <summary>
        /// Property for ShowCustom. When the value changes, updates the control to either allow or disallow the use of custom naming conventions.
        /// </summary>
        public static readonly DependencyProperty UseCustomProperty =
            DependencyProperty.Register("UseCustom", typeof(bool), typeof(SelectorControl), new UIPropertyMetadata(true, (sender, e) =>
            {
                SelectorControl control = (SelectorControl)sender;
                if ((bool)e.NewValue)
                {
                    control.Items.Add(NamingConvention.NamingConventions.Last());
                    control.CustomRow.Visibility = Visibility.Visible;
                }
                else
                {
                    control.Items.RemoveAt(control.Items.Count - 1);
                    if (control.Convention.SelectedValue == null)
                        control.Convention.SelectedIndex = 0;
                    control.CustomRow.Visibility = Visibility.Collapsed;
                }
            }));

        /// <summary>
        /// Get or set whether this control allows the use of a custom namign convention.
        /// </summary>
        public bool UseCustom
        {
            get { return (bool)GetValue(UseCustomProperty); }
            set { SetValue(UseCustomProperty, value); }
        }

        #endregion

        #region Properties

        /// <summary>
        /// The naming convention items.
        /// </summary>
        private ObservableCollection<KeyValuePair<string, NamingConvention>> Items { get; } = new ObservableCollection<KeyValuePair<string, NamingConvention>>(NamingConvention.NamingConventions);

        /// <summary>
        /// Get the selected naming convention, or null if the participant has selected to use a custom naming convention, but has not provided a custom format.
        /// </summary>
        public NamingConvention SelectedConvention
        {
            get
            {
                var convention = (NamingConvention)Convention.SelectedValue;
                if (convention == null && !string.IsNullOrWhiteSpace(CustomFormat.Text))
                    convention = NamingConvention.GetCustomNamingConvention(CustomFormat.Text.Trim());
                if (convention == null)
                    MessageBox.Show("Invalid custom format.", "Invalid Format", MessageBoxButton.OK, MessageBoxImage.Error);
                return convention;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Create the selector control.
        /// </summary>
        public SelectorControl()
        {
            InitializeComponent();

            Convention.ItemsSource = Items;
            Convention.SelectedIndex = 0;
        }

        #endregion

    }
}
