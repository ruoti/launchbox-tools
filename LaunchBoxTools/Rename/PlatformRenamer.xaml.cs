﻿using LaunchBoxTools.Dialogs;
using System;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

using static LaunchBoxTools.LaunchBoxData;

namespace LaunchBoxTools.Rename
{
    /// <summary>
    /// Game tool for renaming platforms.
    /// </summary>
    public sealed partial class PlatformRenamer : UserControl
    {

        #region Static Properties

        /// <summary>
        /// Singleton for this control.
        /// </summary>
        public static PlatformRenamer Instance
        {
            get
            {
                if (instance == null)
                    instance = new PlatformRenamer();
                return instance;
            }
        }
        private static PlatformRenamer instance = null;

        #endregion

        #region Constructor

        /// <summary>
        /// Create the platform renamer control.
        /// </summary>
        private PlatformRenamer()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Rename platforms.
        /// </summary>
        /// <param name="isTest">Whether this is a test.</param>
        /// <param name="useSample">Whether a sample should be used.</param>
        private void Process(bool isTest, bool useSample = false)
        {
            // Changing multiple platforms could be dangerous. Check operation.
            if (SelectedPlatforms.Count() > 1)
            {
                if (MessageBox.Show("You are attempting to rename more than one platform. Are you sure?", "Multiple Platforms?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                    return;
            }

            // Get the settings from the UI.
            var platformName = PlatformName.Text.Trim();
            if (string.IsNullOrEmpty(platformName))
            {
                MessageBox.Show("No new platform name given.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            bool updateMedia = UpdateMedia.IsChecked.Value;
            bool replaceExisting = ReplaceExisting.IsChecked.Value;

            // Create necessary objects.
            StringBuilder testResults = isTest ? new StringBuilder() : null;
            var mediaManager = new MediaManager(replaceExisting);

            // Update the platform in the appropriate games.
            var result = IterateOverGamesBySelectedPlatforms((game) =>
            {
                if (isTest)
                    testResults.AppendFormat("Game {0}\n", GetGameValue(game, "ApplicationPath"));

                UpdateGameValue(game, "Platform", platformName, true, isTest, testResults);

                if (isTest)
                    testResults.Append("\n");
            }, isTest && useSample);
            if (result != IterateOverGamesResult.Finished)
            {
                UndoChanges();
                return;
            }

            throw new NotImplementedException();
            // Update the platform name in EmulatorPlatform elements.
            /*
            if (isTest)
                testResults.Append("\nChanging EmulatorPlatform entries.\n");
            foreach (var emulatorPlatform in Document.Root.Elements("EmulatorPlatform").Where(ep => SelectedPlatforms.Contains(LaunchBoxXml.GetValue(ep, "Platform"))))
                UpdateValue(emulatorPlatform, "Platform", platformName, true, isTest, testResults);

            // Ensure there is a platform item for the new name. Delete the other platforms.
            if (isTest)
                testResults.Append("\nRenaming and removing platform entries.\n");
            if (!Document.Root.Elements("Platform").Any(p => LaunchBoxXml.GetValue(p, "Name").Equals(platformName, StringComparison.InvariantCulture)))
                UpdateValue(Document.Root.Elements("Platform").Where(p => SelectedPlatforms.Contains(LaunchBoxXml.GetValue(p, "Name"))).FirstOrDefault(), "Name", platformName, true, isTest, testResults);
            foreach (var platform in Document.Root.Elements("Platform")
                .Where(p => SelectedPlatforms.Contains(LaunchBoxXml.GetValue(p, "Name")) &&
                            !LaunchBoxXml.GetValue(p, "Name").Equals(platformName, StringComparison.InvariantCulture))
                .ToList())
            {
                if (isTest)
                    testResults.AppendFormat("Remove platform {0}.\n", LaunchBoxXml.GetValue(platform, "Name"));
                else
                    platform.Remove();
            }
            */

            // Move media folders and save or report on changes.
            if (isTest)
            {
                if (updateMedia)
                    testResults.AppendFormat("\n{0}", mediaManager.MovePlatformFiles(platformName, true));
                Dispatcher.Invoke(() => ScrollableTextDialog.ShowDialog(testResults.ToString(), "Test Results"));
            }
            else
            {
                if (updateMedia)
                    mediaManager.MovePlatformFiles(platformName);
                Save();
                MessageBox.Show("Finished!", "Finished", MessageBoxButton.OK);
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Scrape titles.
        /// </summary>
        private void Process_Click(object sender, RoutedEventArgs e)
        {
            Process(false);
        }

        /// <summary>
        /// Test scraping titles.
        /// </summary>
        private void Test_All_Click(object sender, RoutedEventArgs e)
        {
            Process(true, false);
        }

        /// <summary>
        /// Test scraping titles.
        /// </summary>
        private void Test_Sample_Click(object sender, RoutedEventArgs e)
        {
            Process(true, true);
        }

        #endregion

    }
}
