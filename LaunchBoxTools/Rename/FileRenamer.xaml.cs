﻿using LaunchBoxTools.Dialogs;
using LaunchBoxTools.NamingConventions;
using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

using static LaunchBoxTools.LaunchBoxData;

namespace LaunchBoxTools.Rename
{
    /// <summary>
    /// Game tool for renaming files.
    /// </summary>
    public sealed partial class FileRenamer : UserControl
    {

        #region Static Properties

        /// <summary>
        /// Singleton for this control.
        /// </summary>
        public static FileRenamer Instance
        {
            get
            {
                if (instance == null)
                    instance = new FileRenamer();
                return instance;
            }
        }
        private static FileRenamer instance = null;

        /// <summary>
        /// Regex to match disc descriptor in filenames.
        /// </summary>
        private static Regex IgnoreDiscRegex { get; } = new Regex(@"\s[({\[]Dis[ck]\s[^)}\]]+[)}\]]", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        #endregion

        #region Constructor

        /// <summary>
        /// Create the file renamer control.
        /// </summary>
        private FileRenamer()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Rename files.
        /// </summary>
        /// <param name="isTest">Whether this is a test.</param>
        /// <param name="useSample">Whether a sample should be used.</param>
        private void Process(bool isTest, bool useSample = false)
        {
            // Get the settings from the UI.
            var fileNamingConvention = FileConvention.SelectedConvention;
            if (fileNamingConvention == null)
                return;

            var renameDirectory = RenameDirectory.IsChecked.Value;
            var directoryNamingConvention = renameDirectory ? DirectoryConvention.SelectedConvention : null;
            if (renameDirectory && directoryNamingConvention == null)
                return;
            var directoryIgnoreDisc = renameDirectory && DirectoryIgnoreDisc.IsChecked.Value;

            var renameSimilar = RenameSimilar.IsChecked.Value;
            var backupFilename = BackupFilename.IsChecked.Value;
            var replaceExisting = ReplaceExisting.IsChecked.Value;

            // Create necessary objects.
            StringBuilder testResults = isTest ? new StringBuilder() : null;
            var mediaManager = new MediaManager(replaceExisting) { RenameSimilar = renameSimilar };

            // Process the games.
            var result = IterateOverGamesBySelectedPlatforms((game) =>
            {
                // Get the original values.
                var originalPath = GetGameValue(game, "ApplicationPath");
                var originalDirectory = Path.GetDirectoryName(originalPath);
                var originalExtension = Path.GetExtension(originalPath);
                var originalAbsolutePath = Path.GetFullPath(Path.Combine(LaunchBoxDirectory, originalPath));

                // Update the directory if requested.
                var newDirectory = originalDirectory;
                if (renameDirectory)
                {
                    var newDirectoryName = NamingConvention.FormatFilename(directoryNamingConvention.Format(game));
                    if (directoryIgnoreDisc)
                        newDirectoryName = IgnoreDiscRegex.Replace(newDirectoryName, string.Empty);
                    newDirectory = Path.Combine(Path.GetDirectoryName(originalDirectory), newDirectoryName);
                }

                // Get the new filename.
                var newFilename = NamingConvention.FormatFilename(fileNamingConvention.Format(game), "", " -");
                var newPath = Path.Combine(newDirectory, newFilename + originalExtension);
                var newAbsolutePath = Path.GetFullPath(Path.Combine(LaunchBoxDirectory, newPath));

                // Check if the filename has changed.
                if (!originalAbsolutePath.Equals(newAbsolutePath, StringComparison.InvariantCultureIgnoreCase))
                {
                    if (isTest)
                        testResults.AppendFormat("Game {0}\n", GetGameValue(game, "ApplicationPath"));

                    // Update the game with the new path.
                    mediaManager.EnqueueFileRename(originalAbsolutePath, newAbsolutePath);
                    UpdateGameValue(game, "ApplicationPath", newPath, true, isTest, testResults);
                    if (backupFilename)
                        UpdateGameValue(game, "BackupFilename", originalPath, replaceExisting, isTest, testResults);

                    if (isTest)
                        testResults.Append("\n");
                }
            }, isTest && useSample);

            // If the operation was canceled or encountered an error, undo any changes that were made.
            if (result != IterateOverGamesResult.Finished)
            {
                UndoChanges();
            }
            // Save or report on changes.
            else
            {
                if (isTest)
                {
                    testResults.AppendFormat("\n{0}", mediaManager.ProcessFileRenames(true));
                    Dispatcher.Invoke(() => ScrollableTextDialog.ShowDialog(testResults.ToString(), "Test Results"));
                }
                else
                {
                    var conflicts = mediaManager.ProcessFileRenames();
                    if (string.IsNullOrEmpty(conflicts))
                    {
                        Save();
                        MessageBox.Show("Finished!", "Finished", MessageBoxButton.OK);
                    }
                    else
                    {
                        Dispatcher.Invoke(() => ScrollableTextDialog.ShowDialog(testResults.ToString(), "Rename Conflicts"));
                    }
                }
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Process files.
        /// </summary>
        private void Process_Click(object sender, RoutedEventArgs e)
        {
            Process(false);
        }

        /// <summary>
        /// Test scraping titles.
        /// </summary>
        private void Test_All_Click(object sender, RoutedEventArgs e)
        {
            Process(true, false);
        }

        /// <summary>
        /// Test scraping titles.
        /// </summary>
        private void Test_Sample_Click(object sender, RoutedEventArgs e)
        {
            Process(true, true);
        }

        #endregion

    }
}
