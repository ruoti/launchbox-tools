﻿using LaunchBoxTools.Dialogs;
using LaunchBoxTools.NamingConventions;
using System;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

using static LaunchBoxTools.LaunchBoxData;

namespace LaunchBoxTools.Rename
{
    /// <summary>
    /// Game tool for renaming titles.
    /// </summary>
    public sealed partial class TitleRenamer : UserControl
    {

        #region Static Properties

        /// <summary>
        /// Singleton for this control.
        /// </summary>
        public static TitleRenamer Instance
        {
            get
            {
                if (instance == null)
                    instance = new TitleRenamer();
                return instance;
            }
        }
        private static TitleRenamer instance = null;

        #endregion

        #region Constructor

        /// <summary>
        /// Create the title renamer control.
        /// </summary>
        private TitleRenamer()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Rename titles.
        /// </summary>
        /// <param name="isTest">Whether this is a test.</param>
        /// <param name="useSample">Whether a sample should be used.</param>
        private void Process(bool isTest, bool useSample=false)
        {
            // Get the settings from the UI.
            var namingConvention = Convention.SelectedConvention;
            if (namingConvention == null)
                return;

            bool updateMedia = UpdateMedia.IsChecked.Value;
            bool backupTitle = BackupTitle.IsChecked.Value;
            bool updateSortTitle = UpdateSortTitle.IsChecked.Value;

            bool updateOnlyDuplciates = UpdateOnlyDuplicates.IsChecked.Value;
            bool replaceExisting = ReplaceExisting.IsChecked.Value;

            // Create necessary objects.
            StringBuilder testResults = isTest ? new StringBuilder() : null;
            var mediaManager = new MediaManager(replaceExisting);
            var duplicateGames = updateOnlyDuplciates ? GetGameCounts().Where(gc => gc.Value > 1).Select(gc => gc.Key).ToImmutableHashSet() : null;

            // Process the games.
            var result = IterateOverGamesBySelectedPlatforms((game) =>
            {
                var originalTitle = GetGameValue(game, "Title");
                var platform = GetGameValue(game, "Platform");

                // Check to see if we are only updating duplicates.
                if (!updateOnlyDuplciates || duplicateGames.Contains(Tuple.Create(platform, originalTitle)))
                {
                    // Add line in the report for this game.
                    if (isTest)
                        testResults.AppendFormat("Game {0}\n", GetGameValue(game, "ApplicationPath"));

                    // Format and then update the title.
                    var newTitle = namingConvention.Format(game);
                    if (!string.IsNullOrEmpty(newTitle))
                    {
                        if (updateSortTitle)
                            UpdateGameValue(game, "SortTitle", NamingConvention.MoveArticleToBack(newTitle), replaceExisting, isTest, testResults);

                        if (!originalTitle.Equals(newTitle))
                        {
                            // Move media if desired.
                            if (updateMedia)
                                mediaManager.EnqueueTitleRename(game, originalTitle, newTitle);

                            // Backup the original title.
                            if (backupTitle)
                                UpdateGameValue(game, "OriginalTitle", originalTitle, replaceExisting, isTest, testResults);

                            UpdateGameValue(game, "Title", newTitle, true, isTest, testResults);
                        }
                    }

                    if (isTest)
                        testResults.Append("\n");
                }
            }, isTest && useSample);

            // If the operation was canceled or encountered an error, undo any changes that were made.
            if (result != IterateOverGamesResult.Finished)
            {
                UndoChanges();
            }
            // Save or report on changes.
            else
            {
                if (isTest)
                {
                    if (updateMedia)
                        testResults.AppendFormat("\n{0}", mediaManager.ProcessTitleRenames(true));
                    Dispatcher.Invoke(() => ScrollableTextDialog.ShowDialog(testResults.ToString(), "Test Results"));
                }
                else
                {
                    if (updateMedia)
                        mediaManager.ProcessTitleRenames();
                    Save();
                    MessageBox.Show("Finished!", "Finished", MessageBoxButton.OK);
                }
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Process files.
        /// </summary>
        private void Process_Click(object sender, RoutedEventArgs e)
        {
            Process(false);
        }

        /// <summary>
        /// Test scraping titles.
        /// </summary>
        private void Test_All_Click(object sender, RoutedEventArgs e)
        {
            Process(true, false);
        }

        /// <summary>
        /// Test scraping titles.
        /// </summary>
        private void Test_Sample_Click(object sender, RoutedEventArgs e)
        {
            Process(true, true);
        }

        #endregion

    }
}
