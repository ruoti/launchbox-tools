﻿using System.Windows.Controls;

namespace LaunchBoxTools
{
    /// <summary>
    /// Interaction logic for Spacer.xaml
    /// </summary>
    public partial class Spacer : UserControl
    {
        public Spacer()
        {
            InitializeComponent();
        }
    }
}
