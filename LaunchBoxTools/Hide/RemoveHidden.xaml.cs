﻿using LaunchBoxTools.Dialogs;
using System;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

using static LaunchBoxTools.LaunchBoxData;

namespace LaunchBoxTools.Hide
{
    /// <summary>
    /// Game tool for removing hidden games.
    /// </summary>
    public sealed partial class RemoveHidden : UserControl
    {

        #region Static Properties

        /// <summary>
        /// Singleton for this control.
        /// </summary>
        public static RemoveHidden Instance
        {
            get
            {
                if (instance == null)
                    instance = new RemoveHidden();
                return instance;
            }
        }
        private static RemoveHidden instance = null;

        #endregion

        #region Constructor

        /// <summary>
        /// Create the platform renamer control.
        /// </summary>
        private RemoveHidden()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Hide games.
        /// </summary>
        /// <param name="isTest">Whether this is a test.</param>
        /// <param name="useSample">Whether a sample should be used.</param>
        private void Process(bool isTest, bool useSample = false)
        {
            bool updateMedia = UpdateMedia.IsChecked.Value;

            // Create necessary objects.
            StringBuilder testResults = isTest ? new StringBuilder() : null;
            var mediaManager = new MediaManager(true);

            // Update the platform in the appropriate games.
            var result = IterateOverGamesBySelectedPlatforms((game) =>
            {
                if (isTest)
                    testResults.AppendFormat("Game {0}\n", GetGameValue(game, "ApplicationPath"));

                // Hide or unhide the game.
                if (GetGameValue(game, "Hide").Equals("true", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (isTest)
                        testResults.Append("Removing game.\n");
                    else
                        game.Remove();

                    if (updateMedia)
                        mediaManager.EnqueueGameRemoval(game);
                }

                if (isTest)
                    testResults.Append("\n");
            }, isTest && useSample);
            if (result != IterateOverGamesResult.Finished)
            {
                UndoChanges();
                return;
            }

            // Save or report on changes.
            if (isTest)
            {
                if (updateMedia)
                    testResults.AppendFormat("\n{0}", mediaManager.ProcessGameRemovals(true));
                Dispatcher.Invoke(() => ScrollableTextDialog.ShowDialog(testResults.ToString(), "Test Results"));
            }
            else
            {
                if (updateMedia)
                    mediaManager.ProcessGameRemovals();
                Save();
                MessageBox.Show("Finished!", "Finished", MessageBoxButton.OK);
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Scrape titles.
        /// </summary>
        private void Process_Click(object sender, RoutedEventArgs e)
        {
            Process(false);
        }

        /// <summary>
        /// Test scraping titles.
        /// </summary>
        private void Test_All_Click(object sender, RoutedEventArgs e)
        {
            Process(true, false);
        }

        /// <summary>
        /// Test scraping titles.
        /// </summary>
        private void Test_Sample_Click(object sender, RoutedEventArgs e)
        {
            Process(true, true);
        }

        #endregion

    }
}
