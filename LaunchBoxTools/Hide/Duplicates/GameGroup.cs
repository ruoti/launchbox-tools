﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;

namespace LaunchBoxTools.Hide.Duplicates
{
    /// <summary>
    /// Grouping of games based on platform and title.
    /// </summary>
    public sealed class GameGroup : DependencyObject
    {

        #region Dependency Properties

        /// <summary>
        /// IsItemSelected dependency property.
        /// </summary>
        public static readonly DependencyProperty IsItemSelectedProperty =
            DependencyProperty.Register("IsItemSelected", typeof(bool), typeof(GameGroup));

        /// <summary>
        /// Get or set whether any of the child items is selected.
        /// </summary>
        public bool IsItemSelected
        {
            get { return (bool)GetValue(IsItemSelectedProperty); }
            private set { SetValue(IsItemSelectedProperty, value); }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Get the platform name.
        /// </summary>
        public string Platform { get; }

        /// <summary>
        /// Get the game title.
        /// </summary>
        public string Title { get; }

        /// <summary>
        /// Get the items that make up this group.
        /// </summary>
        public ObservableCollection<GameGroupItem> Items { get; }

        #endregion

        #region Constructor

        /// <summary>
        /// Construct the game group.
        /// </summary>
        /// <param name="group">Group of related games.</param>
        public GameGroup(string platform, string title, IEnumerable<GameGroupItem> games)
        {
            Platform = platform;
            Title = title;

            Items = new ObservableCollection<GameGroupItem>(games);
            foreach (var gameItem in Items)
                gameItem.PropertyChanged += GameItem_PropertyChanged;
            GameItem_PropertyChanged(null, null);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Display the game group details.
        /// </summary>
        /// <returns>The game group details.</returns>
        public override string ToString()
        {
            return string.Format("{0}\n{1}\n{2}", Platform, Title, string.Join("\n", Items.Select(i => i.ToString())));
        }

        #endregion

        #region Events

        /// <summary>
        /// Check to see if any item is selected.
        /// </summary>
        private void GameItem_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            IsItemSelected = Items.Any(i => i.IsSelected);
        }

        #endregion

    }

}
