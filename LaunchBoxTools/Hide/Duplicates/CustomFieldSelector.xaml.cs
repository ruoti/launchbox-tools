﻿using System.Collections.Generic;
using System.Linq;

using static LaunchBoxTools.LaunchBoxData;

namespace LaunchBoxTools.Hide.Duplicates
{
    /// <summary>
    /// Duplicate selector based on custom fields.
    /// </summary>
    public partial class CustomFieldSelector : DuplicateSelector
    {

        #region Enum

        /// <summary>
        /// The built in field selection.
        /// </summary>
        private enum CustomFieldType { Equals, NotEquals, Contains, NotContains, Shortest, Longest }

        #endregion

        #region Properties

        /// <summary>
        /// The type of matching to do.
        /// </summary>
        private CustomFieldType MatchType { get; set; }

        /// <summary>
        /// The field to match against.
        /// </summary>
        private string MatchField { get; set; }

        /// <summary>
        /// The value to match against.
        /// </summary>
        private string MatchValue { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Create the region selector.
        /// </summary>
        public CustomFieldSelector()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Read the settings from the user control.
        /// </summary>
        /// <remarks>
        /// Always called from the UI thread, and always called before filter.
        /// </remarks>
        public override void ReadSettings()
        {
            MatchField = Field.Text.Trim();

            if (EqualsType.IsChecked.Value)
            {
                MatchType = CustomFieldType.Equals;
                MatchValue = EqualsTypeValue.Text.Trim();
            }
            else if (NotEqualsType.IsChecked.Value)
            {
                MatchType = CustomFieldType.NotEquals;
                MatchValue = NotEqualsTypeValue.Text.Trim();
            }
            else if (ContainsType.IsChecked.Value)
            {
                MatchType = CustomFieldType.Contains;
                MatchValue = ContainsTypeValue.Text.Trim();
            }
            else if (NotContainsType.IsChecked.Value)
            {
                MatchType = CustomFieldType.NotContains;
                MatchValue = NotContainsTypeValue.Text.Trim();
            }
            else if (PreferShortest.IsChecked.Value)
            {
                MatchType = CustomFieldType.Shortest;
            }
            else if (PreferLongest.IsChecked.Value)
            {
                MatchType = CustomFieldType.Longest;
            }
        }

        /// <summary>
        /// Filter the given game items so that only matching items are selected.
        /// </summary>
        /// <param name="items">Items to filter.</param>
        /// <returns>The game items that match the selector.</returns>
        public override IEnumerable<GameGroupItem> Filter(IEnumerable<GameGroupItem> items)
        {
            if (items.Count() <= 1)
                return items;

            switch(MatchType)
            {
                case CustomFieldType.Equals:
                    return items.Where(i => GetGameValue(i.Game, MatchField).Equals(MatchValue, System.StringComparison.InvariantCultureIgnoreCase));
                case CustomFieldType.NotEquals:
                    return items.Where(i => !GetGameValue(i.Game, MatchField).Equals(MatchValue, System.StringComparison.InvariantCultureIgnoreCase));
                case CustomFieldType.Contains:
                    return items.Where(i => GetGameValue(i.Game, MatchField).IndexOf(MatchValue, System.StringComparison.InvariantCultureIgnoreCase) != -1);
                case CustomFieldType.NotContains:
                    return items.Where(i => GetGameValue(i.Game, MatchField).IndexOf(MatchValue, System.StringComparison.InvariantCultureIgnoreCase) == -1);

                case CustomFieldType.Shortest:
                case CustomFieldType.Longest:
                    var sortedItems =
                        from item in items
                        let length = GetGameValue(item.Game, MatchField).Length
                        orderby length
                        select new { Length = length, Item = item };

                    var matchingLength = (MatchType == CustomFieldType.Shortest ? sortedItems.First() : sortedItems.Last()).Length;
                    return
                        from item in sortedItems
                        where item.Length == matchingLength
                        select item.Item;

                default:
                    return items;
            }
        }

        #endregion

    }
}
