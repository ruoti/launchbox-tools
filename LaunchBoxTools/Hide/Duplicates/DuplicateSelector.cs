﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LaunchBoxTools.Hide.Duplicates
{
    /// <summary>
    /// Duplicate selector base class.
    /// </summary>
    public class DuplicateSelector : ContentControl
    {

        #region Dependency Properties

        /// <summary>
        /// Selector name.
        /// </summary>
        public static readonly DependencyProperty SelectorNameProperty =
            DependencyProperty.Register("SelectorName", typeof(string), typeof(DuplicateSelector), new FrameworkPropertyMetadata(string.Empty));

        /// <summary>
        /// Get or set the name of the selector.
        /// </summary>
        public string SelectorName
        {
            get { return (string)GetValue(SelectorNameProperty); }
            set { SetValue(SelectorNameProperty, value); }
        }

        #endregion

        #region Events

        /// <summary>
        /// Event for when the move up button is clicked.
        /// </summary>
        public static readonly RoutedEvent MoveUpClickEvent =
            EventManager.RegisterRoutedEvent("MoveUpClick", RoutingStrategy.Direct, typeof(RoutedEventHandler), typeof(DuplicateSelector));

        /// <summary>
        /// Fired when the move up button is clicked.
        /// </summary>
        public event RoutedEventHandler MoveUpClick
        {
            add { AddHandler(MoveUpClickEvent, value); }
            remove { RemoveHandler(MoveUpClickEvent, value); }
        }

        /// <summary>
        /// Event for when the move down button is clicked.
        /// </summary>
        public static readonly RoutedEvent MoveDownClickEvent =
            EventManager.RegisterRoutedEvent("MoveDownClick", RoutingStrategy.Direct, typeof(RoutedEventHandler), typeof(DuplicateSelector));

        /// <summary>
        /// Fired when the move down button is clicked.
        /// </summary>
        public event RoutedEventHandler MoveDownClick
        {
            add { AddHandler(MoveDownClickEvent, value); }
            remove { RemoveHandler(MoveDownClickEvent, value); }
        }

        /// <summary>
        /// Event for when the remove button is clicked.
        /// </summary>
        public static readonly RoutedEvent RemoveClickEvent =
            EventManager.RegisterRoutedEvent("RemoveClick", RoutingStrategy.Direct, typeof(RoutedEventHandler), typeof(DuplicateSelector));

        /// <summary>
        /// Fired when the remove button is clicked.
        /// </summary>
        public event RoutedEventHandler RemoveClick
        {
            add { AddHandler(RemoveClickEvent, value); }
            remove { RemoveHandler(RemoveClickEvent, value); }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Static constructor for the selector.
        /// </summary>
        static DuplicateSelector()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(DuplicateSelector), new FrameworkPropertyMetadata(typeof(DuplicateSelector)));
        }

        /// <summary>
        /// Create the selector.
        /// </summary>
        public DuplicateSelector() { }

        #endregion

        #region Methods

        /// <summary>
        /// Apply the template and hookup events.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            // Grab the buttons and hookup the events.
            var moveUp = Template.FindName("MoveUp", this) as Button;
            if (moveUp != null)
                moveUp.Click += MoveUp_Click;

            var moveDown = Template.FindName("MoveDown", this) as Button;
            if (moveDown != null)
                moveDown.Click += MoveDown_Click;

            var remove = Template.FindName("Remove", this) as Button;
            if (remove != null)
                remove.Click += Remove_Click;
        }

        /// <summary>
        /// Fired when the move up button is clicked.
        /// </summary>
        private void MoveUp_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(MoveUpClickEvent, this));
        }

        /// <summary>
        /// Fired when the move down button is clicked.
        /// </summary>
        private void MoveDown_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(MoveDownClickEvent, this));
        }

        /// <summary>
        /// Fired when the remove button is clicked.
        /// </summary>
        private void Remove_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(RemoveClickEvent, this));
        }

        /// <summary>
        /// Read the settings from the user control.
        /// </summary>
        /// <remarks>
        /// Always called from the UI thread, and always called before filter.
        /// </remarks>
        public virtual void ReadSettings()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Filter the given game items so that only matching items are selected.
        /// </summary>
        /// <param name="items">Items to filter.</param>
        /// <returns>The game items that match the selector.</returns>
        public virtual IEnumerable<GameGroupItem> Filter(IEnumerable<GameGroupItem> items)
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}
