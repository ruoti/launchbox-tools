﻿using System.Collections.Generic;
using System.Linq;

using static LaunchBoxTools.LaunchBoxData;

namespace LaunchBoxTools.Hide.Duplicates
{
    /// <summary>
    /// Duplicate selector based on built in fields.
    /// </summary>
    public partial class BuiltInFieldSelector : DuplicateSelector
    {

        #region Enum

        /// <summary>
        /// The built in field selection.
        /// </summary>
        private enum BuiltInFieldType { Equals, NotEquals, Contains, NotContains, Shortest, Longest }

        #endregion

        #region Properties

        /// <summary>
        /// The type of matching to do.
        /// </summary>
        private BuiltInFieldType MatchType { get; set; }

        /// <summary>
        /// The field to match against.
        /// </summary>
        private string MatchField { get; set; }

        /// <summary>
        /// The value to match against.
        /// </summary>
        private string MatchValue { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Create the region selector.
        /// </summary>
        public BuiltInFieldSelector()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Read the settings from the user control.
        /// </summary>
        /// <remarks>
        /// Always called from the UI thread, and always called before filter.
        /// </remarks>
        public override void ReadSettings()
        {
            MatchField = (string)Field.SelectedValue;

            if (EqualsType.IsChecked.Value)
            {
                MatchType = BuiltInFieldType.Equals;
                MatchValue = EqualsTypeValue.Text.Trim();
            }
            else if (NotEqualsType.IsChecked.Value)
            {
                MatchType = BuiltInFieldType.NotEquals;
                MatchValue = NotEqualsTypeValue.Text.Trim();
            }
            else if (ContainsType.IsChecked.Value)
            {
                MatchType = BuiltInFieldType.Contains;
                MatchValue = ContainsTypeValue.Text.Trim();
            }
            else if (NotContainsType.IsChecked.Value)
            {
                MatchType = BuiltInFieldType.NotContains;
                MatchValue = NotContainsTypeValue.Text.Trim();
            }
            else if (PreferShortest.IsChecked.Value)
            {
                MatchType = BuiltInFieldType.Shortest;
            }
            else if (PreferLongest.IsChecked.Value)
            {
                MatchType = BuiltInFieldType.Longest;
            }
        }

        /// <summary>
        /// Filter the given game items so that only matching items are selected.
        /// </summary>
        /// <param name="items">Items to filter.</param>
        /// <returns>The game items that match the selector.</returns>
        public override IEnumerable<GameGroupItem> Filter(IEnumerable<GameGroupItem> items)
        {
            if (items.Count() <= 1)
                return items;

            switch (MatchType)
            {
                case BuiltInFieldType.Equals:
                    return items.Where(i => GetGameValue(i.Game, MatchField).Equals(MatchValue, System.StringComparison.InvariantCultureIgnoreCase));
                case BuiltInFieldType.NotEquals:
                    return items.Where(i => !GetGameValue(i.Game, MatchField).Equals(MatchValue, System.StringComparison.InvariantCultureIgnoreCase));
                case BuiltInFieldType.Contains:
                    return items.Where(i => GetGameValue(i.Game, MatchField).IndexOf(MatchValue, System.StringComparison.InvariantCultureIgnoreCase) != -1);
                case BuiltInFieldType.NotContains:
                    return items.Where(i => GetGameValue(i.Game, MatchField).IndexOf(MatchValue, System.StringComparison.InvariantCultureIgnoreCase) == -1);

                case BuiltInFieldType.Shortest:
                case BuiltInFieldType.Longest:
                    var sortedItems =
                        from item in items
                        let length = GetGameValue(item.Game, MatchField).Length
                        orderby length
                        select new { Length = length, Item = item };

                    var matchingLength = (MatchType == BuiltInFieldType.Shortest ? sortedItems.First() : sortedItems.Last()).Length;
                    return
                        from item in sortedItems
                        where item.Length == matchingLength
                        select item.Item;

                default:
                    return items;
            }
        }

        #endregion

    }
}
