﻿using System;
using System.Collections.Generic;
using System.Linq;

using static LaunchBoxTools.LaunchBoxData;

namespace LaunchBoxTools.Hide.Duplicates
{
    /// <summary>
    /// Duplicate selector based on version.
    /// </summary>
    public partial class ReleaseDateSelector : DuplicateSelector
    {

        #region ReleaseDateComparer Inner Class

        /// <summary>
        /// Comparer for version strings.
        /// </summary>
        public class ReleaseDateComparer : IComparer<string>
        {
            /// <summary>
            /// Compare two version strings.
            /// </summary>
            /// <param name="x">First version.</param>
            /// <param name="y">Second version.</param>
            /// <returns>-1 when x is less than y, 0 if they are equal, and 1 if x is greater than y.</returns>
            public int Compare(string x, string y)
            {
                // Check equality.
                if (x.Equals(y, StringComparison.InvariantCultureIgnoreCase))
                    return 0;

                // Try to parse the date.
                DateTime xDateTime, yDateTime;
                bool xHasDateTime = DateTime.TryParse(x, out xDateTime);
                bool yHasDateTime = DateTime.TryParse(y, out yDateTime);
                if (xHasDateTime || yHasDateTime)
                {
                    if (!xHasDateTime)
                        return -1;
                    if (!yHasDateTime)
                        return 1;

                    return xDateTime.CompareTo(yDateTime);
                }

                // Neither is a valid date time, so just ignore them.
                return 0;
            }
        }


        #endregion

        #region Properties

        /// <summary>
        /// Whether to use the highest date.
        /// </summary>
        private bool PreferHighestDate { get; set; }

        /// <summary>
        /// Whether to use the lowest date.
        /// </summary>
        private bool PreferLowestDate { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Create the version selector.
        /// </summary>
        public ReleaseDateSelector()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Read the settings from the user control.
        /// </summary>
        /// <remarks>
        /// Always called from the UI thread, and always called before filter.
        /// </remarks>
        public override void ReadSettings()
        {
            PreferHighestDate = PreferHighest.IsChecked.Value;
            PreferLowestDate = PreferLowest.IsChecked.Value;
        }

        /// <summary>
        /// Filter the given game items so that only matching items are selected.
        /// </summary>
        /// <param name="items">Items to filter.</param>
        /// <returns>The game items that match the selector.</returns>
        public override IEnumerable<GameGroupItem> Filter(IEnumerable<GameGroupItem> items)
        {
            if (items.Count() <= 1)
                return items;

            if (PreferHighestDate)
            {
                var highestDate = items.OrderByDescending(i => GetGameValue(i.Game, "ReleaseDate"), new ReleaseDateComparer()).First().Version;
                return items.Where(i => GetGameValue(i.Game, "ReleaseDate").Equals(highestDate, StringComparison.InvariantCultureIgnoreCase));
            }
            else if (PreferLowestDate)
            {
                var lowestDate = items.OrderBy(i => GetGameValue(i.Game, "ReleaseDate"), new ReleaseDateComparer()).First().Version;
                return items.Where(i => GetGameValue(i.Game, "ReleaseDate").Equals(lowestDate, StringComparison.InvariantCultureIgnoreCase));
            }
            else
                return items;
        }

        #endregion

    }
}
