﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace LaunchBoxTools.Hide.Duplicates
{
    /// <summary>
    /// Duplicate selector based on version.
    /// </summary>
    public partial class VersionSelector : DuplicateSelector
    {

        #region VersionComparer Inner Class

        /// <summary>
        /// Comparer for version strings.
        /// </summary>
        public class VersionComparer : IComparer<string>
        {

            /// <summary>
            /// The different version types.
            /// </summary>
            private enum VersionType { Proto = 0, Alpha = 1, Beta = 2, Sample=3, Version = 4, Revision = 5 }

            /// <summary>
            /// Regex for matching the version.
            /// </summary>
            private static Regex VersionRegex { get; } = new Regex(@"^( v?\d(\.\d+)? | Rev\s[^)]+ | (Proto|Alpha|Beta|Sample)\s*[^)]* )$", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);

            /// <summary>
            /// Regex for matching numbers.
            /// </summary>
            private static Regex NumbersRegex { get; } = new Regex(@"\d(\.\d+)?", RegexOptions.Compiled);

            /// <summary>
            /// Compare two version strings.
            /// </summary>
            /// <param name="x">First version.</param>
            /// <param name="y">Second version.</param>
            /// <returns>-1 when x is less than y, 0 if they are equal, and 1 if x is greater than y.</returns>
            public int Compare(string x, string y)
            {
                // Check equality.
                if (x.Equals(y, StringComparison.InvariantCultureIgnoreCase))
                    return 0;

                // Set the default versions.
                if (string.IsNullOrWhiteSpace(x))
                    x = "1";
                if (string.IsNullOrWhiteSpace(y))
                    y = "1";

                // Try to grab version types.
                var xMatch = VersionRegex.Match(x);
                var yMatch = VersionRegex.Match(y);
                if (xMatch.Success || yMatch.Success)
                {
                    if (!xMatch.Success)
                        return -1;
                    if (!yMatch.Success)
                        return 1;

                    var xVersionType = GetVersionType(xMatch.Value.Trim());
                    var yVersionType = GetVersionType(yMatch.Value.Trim());

                    // If different version types, then return the higher version type.
                    if (xVersionType != yVersionType)
                        return ((int)xVersionType) - ((int)yVersionType);
                }

                // Try to parse a number.
                xMatch = NumbersRegex.Match(x);
                yMatch = NumbersRegex.Match(y);
                if (xMatch.Success || yMatch.Success)
                {
                    if (!xMatch.Success)
                        return -1;
                    if (!yMatch.Success)
                        return 1;

                    double xVersion, yVersion;
                    bool xHasVersion = double.TryParse(xMatch.Value, out xVersion);
                    bool yHasVersion = double.TryParse(yMatch.Value, out yVersion);
                    if (xHasVersion || yHasVersion)
                    {
                        if (!xHasVersion)
                            return -1;
                        if (!yHasVersion)
                            return 1;

                        if (xVersion < yVersion)
                            return -1;
                        if (xVersion > yVersion)
                            return 1;
                        return 0;
                    }
                }


                // Resort to string comparison.
                return StringComparer.InvariantCultureIgnoreCase.Compare(x, y);
            }

            /// <summary>
            /// Get the version type.
            /// </summary>
            /// <param name="str">Version string to parse.</param>
            /// <returns>Version type.</returns>
            private static VersionType GetVersionType(string str)
            {
                if (str.StartsWith("Proto"))
                    return VersionType.Proto;
                if (str.StartsWith("Alpha"))
                    return VersionType.Alpha;
                if (str.StartsWith("Beta"))
                    return VersionType.Beta;
                if (str.StartsWith("Sample"))
                    return VersionType.Sample;
                if (str.StartsWith("Rev"))
                    return VersionType.Revision;
                if (str.StartsWith("v"))
                    return VersionType.Version;
                else
                    return VersionType.Version;
            }

        }

        #endregion

        #region Properties

        /// <summary>
        /// Whether to use the highest version.
        /// </summary>
        private bool PreferHighestVersion { get; set; }

        /// <summary>
        /// Whether to use the lowest version.
        /// </summary>
        private bool PreferLowestVersion { get; set; }

        /// <summary>
        /// Whether to use a specific version.
        /// </summary>
        private bool PreferSpecificVersion { get; set; }

        /// <summary>
        /// The value of the specific version.
        /// </summary>
        private string PreferSpecificVersionValue { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Create the version selector.
        /// </summary>
        public VersionSelector()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Read the settings from the user control.
        /// </summary>
        /// <remarks>
        /// Always called from the UI thread, and always called before filter.
        /// </remarks>
        public override void ReadSettings()
        {
            PreferHighestVersion = PreferHighest.IsChecked.Value;
            PreferLowestVersion = PreferLowest.IsChecked.Value;
            PreferSpecificVersion = PreferSpecific.IsChecked.Value;
            PreferSpecificVersionValue = PreferSpecificValue.Text.Trim();
        }

        /// <summary>
        /// Filter the given game items so that only matching items are selected.
        /// </summary>
        /// <param name="items">Items to filter.</param>
        /// <returns>The game items that match the selector.</returns>
        public override IEnumerable<GameGroupItem> Filter(IEnumerable<GameGroupItem> items)
        {
            if (items.Count() <= 1)
                return items;

            if (PreferHighestVersion)
            {
                var highestVersion = items.OrderByDescending(i => i.Version, new VersionComparer()).First().Version;
                return items.Where(i => i.Version.Equals(highestVersion, StringComparison.InvariantCultureIgnoreCase));
            }
            else if (PreferLowestVersion)
            {
                var lowestVersion = items.OrderBy(i => i.Version, new VersionComparer()).First().Version;
                return items.Where(i => i.Version.Equals(lowestVersion, StringComparison.InvariantCultureIgnoreCase));
            }
            else if (PreferSpecificVersion)
                return items.Where(i => i.Version.Equals(PreferSpecificVersionValue, StringComparison.InvariantCultureIgnoreCase));
            else
                return items;
        }

        #endregion

    }
}
