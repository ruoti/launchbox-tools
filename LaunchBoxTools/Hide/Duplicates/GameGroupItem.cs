﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Xml.Linq;

using static LaunchBoxTools.LaunchBoxData;

namespace LaunchBoxTools.Hide.Duplicates
{
    /// <summary>
    /// An item in a <see cref="GameGroup"/>.
    /// </summary>
    public sealed class GameGroupItem : DependencyObject, INotifyPropertyChanged
    {

        #region Dependency Properties

        /// <summary>
        /// IsSelected dependency property.
        /// </summary>
        public static readonly DependencyProperty IsSelectedProperty =
            DependencyProperty.Register("IsSelected", typeof(bool), typeof(GameGroupItem),
                new UIPropertyMetadata(false, (sender, e) => ((GameGroupItem)sender).PropertyChanged(sender, new PropertyChangedEventArgs(IsSelectedProperty.Name))));

        /// <summary>
        /// Get or set whether the platform is selected.
        /// </summary>
        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Get the game.
        /// </summary>
        public XElement Game { get; }

        /// <summary>
        /// Get the name of the group this item is in.
        /// </summary>
        public string GroupName { get { return string.Format("{0}-{1}", GetGameValue(Game, "Platform"), GetGameValue(Game, "Title")); } }

        /// <summary>
        /// Get the game's region.
        /// </summary>
        public string Region { get { return GetGameValue(Game, "Region"); } }

        /// <summary>
        /// Get the game's version.
        /// </summary>
        public string Version { get { return GetGameValue(Game, "Version"); } }

        /// <summary>
        /// Get the game's language.
        /// </summary>
        public string Language { get { return GetGameValue(Game, "Language"); } }

        /// <summary>
        /// Get the game's path.
        /// </summary>
        public string Path { get { return GetGameValue(Game, "ApplicationPath"); } }

        /// <summary>
        /// Get the game's XML.
        /// </summary>
        public string FullXML { get { return Game.ToString(); } }

        /// <summary>
        /// Fired when IsSelected is changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Constructor

        /// <summary>
        /// Construct the game item.
        /// </summary>
        /// <param name="game">The game.</param>
        /// <param name="isSelected">Whether the game is selected.</param>
        public GameGroupItem(XElement game, bool isSelected)
        {
            Game = game;
            IsSelected = isSelected;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Display the game path.
        /// </summary>
        /// <returns>Game path.</returns>
        public override string ToString()
        {
            return Path.ToString();
        }

        #endregion

    }
}
