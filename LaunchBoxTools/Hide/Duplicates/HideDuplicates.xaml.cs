﻿using LaunchBoxTools.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

using static LaunchBoxTools.LaunchBoxData;

namespace LaunchBoxTools.Hide.Duplicates
{
    /// <summary>
    /// Game tool for removing duplicate games (as defined by platform and title).
    /// </summary>
    public sealed partial class HideDuplicates : UserControl
    {

        #region Static Properties

        /// <summary>
        /// Singleton for this control.
        /// </summary>
        public static HideDuplicates Instance
        {
            get
            {
                if (instance == null)
                    instance = new HideDuplicates();
                return instance;
            }
        }
        private static HideDuplicates instance = null;

        /// <summary>
        /// The selectors used in for selection.
        /// </summary>
        private ObservableCollection<DuplicateSelector> Selectors { get; } = new ObservableCollection<DuplicateSelector>();

        #endregion

        #region Constructor

        /// <summary>
        /// Create the deduplicator control.
        /// </summary>
        private HideDuplicates()
        {
            InitializeComponent();
            SelectorsPanel.ItemsSource = Selectors;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Hide duplicates.
        /// </summary>
        /// <param name="isTest">Whether this is a test.</param>
        /// <param name="useSample">Whether a sample should be used.</param>
        private void Process(bool isTest, bool useSample = false)
        {
            // Retrieve settings from UI.
            foreach (var selector in Selectors)
                selector.ReadSettings();
            
            // Retrieve the duplicate games.
            var useHidden = IncludeHidden.IsChecked.Value;
            var duplicateGames = (
                from game in (useSample ? GetGamesBySelectedPlatforms().Sample() : GetGamesBySelectedPlatforms())
                where useHidden || GetGameValue(game, "Hide").Equals("False", System.StringComparison.InvariantCultureIgnoreCase)
                group game by new { Platform = GetGameValue(game, "Platform"), Title = GetGameValue(game, "Title") } into pairing
                where pairing.Count() > 1
                orderby pairing.Key.Platform, pairing.Key.Title
                select new GameGroup(pairing.Key.Platform, pairing.Key.Title, from item in pairing select new GameGroupItem(item, false))
                ).ToList();

            // If there are no duplicates then there is nothing to do.
            if (duplicateGames.Count == 0)
            {
                MessageBox.Show("No duplicate games.", "No Duplicates", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            // Iterate over the duplicates and attempt to select game to show.
            bool finished = false;
            ProgressDialog.ShowDialog(dialog =>
            {
                double totalIterations = duplicateGames.Count, currentIteration = 0;

                foreach (var group in duplicateGames)
                {
                    // Check for cancelation.
                    if (dialog.CancelRequested)
                        return;

                    // Report on the current prgoress.
                    currentIteration++;
                    dialog.ReportProgress((currentIteration / totalIterations) * 100, subText: string.Format("Game group {0} of {1}.", currentIteration, totalIterations));

                    // The possible candidate game items.
                    var candidateGameItems = new List<GameGroupItem>(group.Items);

                    // Iterate over the various selectors according to priority.
                    foreach (var selector in Selectors)
                    {
                        var newCadidates = selector.Filter(candidateGameItems);

                        // If nothing matched, ignore the results and try to filter using a different filter.
                        if (!newCadidates.Any())
                            continue;
                                
                        // If only one cadidate found, then it is the game we want to select.
                        if (newCadidates.Count() == 1)
                        {
                            Dispatcher.Invoke(() => newCadidates.First().IsSelected = true);
                            break;
                        }
                        // Otherwise, prune the list and continue.
                        else 
                            candidateGameItems = newCadidates.ToList();
                    }
                }
                finished = true;
            }, "Processing Games", "Auto-selecting duplicate games to hide...", null, allowCancel: true);

            if (!finished)
            {
                UndoChanges();
            }
            else
            {
                // Display remaining duplicates, and get feedback from the user.
                var results = GameGroupSelectionDialog.ShowDialog(duplicateGames);
                StringBuilder testResults = isTest ? new StringBuilder() : null;

                if (results != null)
                {
                    // Save changes made by the user.
                    foreach(var gameGroup in results)
                    {
                        if (gameGroup.IsItemSelected)
                        {
                            foreach (var gameItem in gameGroup.Items)
                            {
                                if (isTest)
                                    testResults.AppendFormat("Game {0}\n", GetGameValue(gameItem.Game, "ApplicationPath"));
                                UpdateGameValue(gameItem.Game, "Hide", gameItem.IsSelected ? "false" : "true", true, isTest, testResults);
                                if (isTest)
                                    testResults.Append("\n");
                            }
                        }
                    }

                    // Save or show results.
                    if (isTest)
                        Dispatcher.Invoke(() => ScrollableTextDialog.ShowDialog(testResults.ToString(), "Test Results"));
                    else
                    {
                        Save();
                        MessageBox.Show("Finished!", "Finished", MessageBoxButton.OK);
                    }
                }
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Add the selected filter.
        /// </summary>
        private void AddFilter_Click(object sender, RoutedEventArgs e)
        {
            DuplicateSelector selector = null;

            if (AddFilterCombo.SelectedValue == BuiltInFieldFilter)
                selector = new BuiltInFieldSelector();
            else if (AddFilterCombo.SelectedValue == CustomFieldFilter)
                selector = new CustomFieldSelector();
            else if (AddFilterCombo.SelectedValue == ReleaseDateFilter)
                selector = new ReleaseDateSelector();
            else if (AddFilterCombo.SelectedValue == VersionFilter)
                selector = new VersionSelector();
            else
                throw new ArgumentException("Invalid filter type.");

            // Add event handlers.
            selector.MoveUpClick += Selector_MoveUpClick;
            selector.MoveDownClick += Selector_MoveDownClick;
            selector.RemoveClick += Selector_RemoveClick;

            // Add control.
            Selectors.Add(selector);
        }

        /// <summary>
        /// Remove the selector.
        /// </summary>
        private void Selector_RemoveClick(object sender, RoutedEventArgs e)
        {
            var index = Selectors.IndexOf((DuplicateSelector)sender);
            if (index == -1)
                return;

            Selectors.RemoveAt(index);
        }

        /// <summary>
        /// Move up the selector.
        /// </summary>
        private void Selector_MoveUpClick(object sender, RoutedEventArgs e)
        {
            var index = Selectors.IndexOf((DuplicateSelector)sender);
            if (index == -1)
                return;
            
            // Can't be moved up.
            if (index == 0)
                return;

            // Move the selector up.
            Selectors.Move(index, index - 1);
        }

        /// <summary>
        /// Move down the selector.
        /// </summary>
        private void Selector_MoveDownClick(object sender, RoutedEventArgs e)
        {
            var index = Selectors.IndexOf((DuplicateSelector)sender);
            if (index == -1)
                return;

            // Can't be moved up.
            if (index == (Selectors.Count - 1))
                return;

            // Move the selector up.
            Selectors.Move(index, index + 1);
        }

        /// <summary>
        /// Process files.
        /// </summary>
        private void Process_Click(object sender, RoutedEventArgs e)
        {
            Process(false);
        }

        /// <summary>
        /// Test scraping titles.
        /// </summary>
        private void Test_All_Click(object sender, RoutedEventArgs e)
        {
            Process(true, false);
        }

        /// <summary>
        /// Test scraping titles.
        /// </summary>
        private void Test_Sample_Click(object sender, RoutedEventArgs e)
        {
            Process(true, true);
        }

        #endregion

    }
}
