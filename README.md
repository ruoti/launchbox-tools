# README #

This project is a collection of utilities that modify the game library for LaunchBox. It includes tools that modify games (e.g., hide duplicate games) and also that scrape additional metadata for games (e.g., System INI).

Details about the tool itself can be found in the [wiki](https://bitbucket.org/mathflair/launchboxtools/wiki/Home).

If you would like to contribute to this project, feel free to fork the repository and send me pull requests. If you want something added, but don't have the time or programming experience to add it to the tool, let me know and  I'll see if I have time to help you out.

## How do I get set up? ##

If you want to just download and use LaunchBox Tools go to the [downloads](https://bitbucket.org/mathflair/launchboxannotator/downloads) page.

If you want to build the project you will need to use Visual Studio. The project is written using C# 6.0, so you will either need to use Visual Studio 2015 or add C# 6.0 support for Visual Studio 2013. The free community version of Visual Studio will build this project just fine.

## Roadmap ##

####v1.3####
* TOSEC naming convention.
* Good Codes naming convention.
* IMDB scraper.

####Future (as interest dictates)####
* Image scraping for System INI scraper.
* Better handling of the Emuxtras Synopsis files, include CRC matching and image retrieval.